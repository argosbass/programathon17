<?php
include_once("seguridad.php");
include_once('vendor/adodb/adodb.inc.php');
include_once("vendor/config.php");
$sql = sprintf("SELECT * from usuarios");
$sql = $yoelijo->Prepare($sql);
$Recordset	= $yoelijo->Execute($sql) or DIE($yoelijo->ErrorMsg())	
?><!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>getCode()</title>
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="css/business-frontpage.css" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
    <style>
    .navbar-toggler {
        z-index: 1;
    }
    
    @media (max-width: 576px) {
        nav > .container {
            width: 100%;
        }
    }
    </style>
    <script>
        function confirmar(id,estado){
            if(estado!=2){
                var r = confirm("Esta seguro que desea eliminar el usuario "+id);
                if (r == true) {
                    window.location = "eliminarUsuarios.php?id="+id;
                }
            }else{
                alert('No se puede elminar un usuario que tenga un proceso de votación activo');
            }
        }
        </script>

</head>

<body>

    <!-- Navigation -->
    <nav class="navbar fixed-top navbar-toggleable-md navbar-inverse bg-inverse">
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarExample" aria-controls="navbarExample" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="container">
            <a class="navbar-brand" href="#">Start Bootstrap</a>
            <div class="collapse navbar-collapse" id="navbarExample">
                <ul class="navbar-nav ml-auto">
                <?php include_once("menu.php"); ?>
                </ul>
            </div>
        </div>
    </nav>

    <!-- Page Content -->
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1>Lista Usuarios</h1>
                <br>
                <?php if($_GET['msj']==1){ ?>
                <div class="alert alert-success">
                    <strong>Aplicado!</strong> Usuario Eliminado.
                  </div>
                <?php } ?>
                <?php if($_GET['msj']==2){ ?>
                <div class="alert alert-success">
                    <strong>Aplicado!</strong> Usuario modificado.
                  </div>
                <?php } ?>
                <?php if($_GET['msj']==3){ ?>
                <div class="alert alert-success">
                    <strong>Aplicado!</strong> Usuario creado.
                  </div>
                <?php } ?>
                 <a href="crear_usuario.php" class="btn btn-info" role="button">Crear Usuario</a>
                  <div class="table-responsive">
                    <table id="datatable1" class="table table-striped table-bordered" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Nombre</th>
                                <th>Tipo</th>
                                <th>Correo</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                                <th>#</th>
                                <th>Nombre</th>
                                <th>Tipo</th>
                                <th>Correo</th>
                                <th></th>
                            </tr>
                        </tfoot>
                        <tbody>
                            <?php while (!$Recordset->EOF) { ?>
                            <tr>
                                <td><?php echo $Recordset->Fields("id");?></td>
                                <td><?php echo $Recordset->Fields("nombre");?></td>
                                <td><?php if($Recordset->Fields("tipoUsuario")==1){ echo "Administrador"; }else{ echo "Usuario"; }?></td>
                                <td><?php echo $Recordset->Fields("correo");?></td>
                                <td><a href="editarUsuarios.php?id=<?php echo $Recordset->Fields("id");?>"><i class="fa fa-edit"></i></a> <a href="#" onclick="javascript: confirmar(<?php echo $Recordset->Fields("id");?>,<?php echo $Recordset->Fields("estado");?>);"><i class="fa fa-remove"></i></a></td>
                            </tr>
                            <?php $Recordset->MoveNext();} ?> 
                        </tbody>
                    </table>
                </div>                          
            </div>
        </div>
    </div>
    <!-- /.container -->

    <!-- Footer -->
    <footer class="py-5 bg-inverse">
        <div class="container">
            <p class="m-0 text-center text-white">Copyright &copy; Your Website 2017</p>
        </div>
        <!-- /.container -->
    </footer>

    <!-- Bootstrap core JavaScript -->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/tether/tether.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.min.js"></script>

</body>

</html>
