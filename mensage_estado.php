<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>getCode()</title>

    <!-- Bootstrap core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/business-frontpage.css" rel="stylesheet">

    <!-- Temporary navbar container fix -->
    <style>
    .navbar-toggler {
        z-index: 1;
    }
    
	
    footer {
      position: absolute;
      bottom: 0;
      width: 100%; 
    }
    
   .agregar-nombre {
     margin-top: 10%; 
   }

   .continuar {
      text-align: center;
   }
   
   .menu-header {
     float:right;
   } 

   .navbar.fixed-top {
      position: relative;
   }

   .container h2 {
    margin-top: 50px;
    text-align: center;
  }
   	
    @media (max-width: 576px) {
      .display-4{
        font-size: 200%;
      }
    }
 
    @media (max-width: 576px) {
        nav > .container {
            width: 100%;
        }
    }
    </style>

</head>

<body>

    <!-- Navigation -->
    <nav class="navbar fixed-top navbar-toggleable-md navbar-inverse bg-inverse">
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarExample" aria-controls="navbarExample" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="container">
            <a class="navbar-brand" href="#">Evento</a>
            <div class="collapse navbar-collapse" id="navbarExample">
            </div>
        </div>
    </nav>

    <!-- Page Content -->
    <div class="container">
            <?php if(isset($_GET["status"]) && $_GET["status"] == 1): ?> 
              <h2>El evento ha terminado</h2>
            <?php else: ?>
              <h2>El evento no ha iniciado</h2>
            <?php endif; ?>

    </div>
    <!-- /.container -->

    <!-- Footer -->
    <footer class="py-5 bg-inverse">
        <div class="container">
            <p class="m-0 text-center text-white">Copyright &copy; Your Website 2017</p>
        </div>
        <!-- /.container -->
    </footer>

    <!-- Bootstrap core JavaScript -->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/tether/tether.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.min.js"></script>

</body>

</html>
