App = {
    web3Provider: null,
    contracts: {},
    init: function () {
        return App.initWeb3();
    },
    initWeb3: function () {

        // Inicializa web3 y apunta el proveedor hacia testRPC.
        if (typeof web3 !== 'undefined')
        {
            App.web3Provider = web3.currentProvider;
            web3 = new Web3(web3.currentProvider);
        } else
        {
            // escoja el proveedor específico de Web3.providers
            App.web3Provider = new Web3.providers.HttpProvider('http://localhost:8545');
            web3 = new Web3(App.web3Provider);
        }

        return App.initContract();
    },
    initContract: function () {

        $.getJSON('build/contracts/Voting.json'
                , function (data)
                {
                    // Get the necessary contract artifact file and instantiate it with truffle-contract.
                    var AdoptionArtifact = data;

                    App.contracts.Voting = TruffleContract(AdoptionArtifact);

                    // Set the provider for our contract.
                    App.contracts.Voting.setProvider(App.web3Provider);


                });

        return App.bindEvents();
    },
    bindEvents: function () {

          App.handleCrearTokens();
    },

    handleCrearTokens: function ( ) {


        console.log("handleCrearTokens");

        

        qtytokens  = $('#qty-tokens').val();
        IdProposal  = $('#LastIdProposal').val();
        
console.log("qtytokens",qtytokens);
console.log("IdProposal",IdProposal);

        var votingInstance;

        web3.eth.getAccounts(function (error, accounts)
        {
            if (error)
            {
                console.log(error);
            }

            var account = accounts[0];
            console.log("handleCrearTokens account", account);

            App.contracts.Voting.deployed().then(function (instance)
            {

                votingInstance = instance;

                return votingInstance.newToken( IdProposal , qtytokens , {gas: 300000, from: account});

            }).then(function (result)
            {
                console.log("handleCrearTokens result", result);
                return App.getTokens();

            }).catch(function (err) {

                console.log("handleCrearTokens ERROR", err.message);

            });
        });

        
    },
    getTokens: function () {


        console.log("getTokens");
        
        IdProposal  = $('#LastIdProposal').val();

        var votingInstance;

        web3.eth.getAccounts(function (error, accounts)
        {
            if (error)
            {
                console.log(error);
            }

            var account = accounts[0];
            console.log("getTokens account", account);

            App.contracts.Voting.deployed().then(function (instance)
            {

                votingInstance = instance;

                //  return votingInstance.getTokens( "0" , {gas: 300000, from: account});


                    votingInstance.getTokens.call( IdProposal ).then(function (v) {

                    value = v.toString();
                    console.log("val", value );

                    

                    $('#textarea-tokens').val(value);
                });


            })/*.then(function (result)
             {
             console.log("getTokens resulttoString()", result.toString());
             console.log("getTokens result", result);
             
             
             })*/.catch(function (err) {

                console.log("getTokens ERROR", err.message);

            });
        });


    },
   
};

$(function () {
    $(window).load(function () {
        App.init();
    });
});

