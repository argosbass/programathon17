(function($){
    $(document).ready(function() {

        $('.addButton').click( function() {

                var $template = $('#optionTemplate'),
                    $clone    = $template
                                    .clone()
                                    .removeClass('hide')
                                    .removeAttr('id')
                                    .insertBefore($template);


            });


        $(document).on('click', '.removeButton', function(e) {
            
            var $row    = $(this).parents('.form-group').remove();
               
        });

        $('.addButtonmultiple').click( function() {

                var $template = $('#multipleoptionTemplate'),
                    $clone    = $template
                                    .clone()
                                    .removeClass('hide')
                                    .removeAttr('id')
                                    .insertBefore($template);


            });


        $(document).on('click', '.removeButton', function(e) {
            
            var $row    = $(this).parents('.form-group').remove();
               
        });


        $("input[type=radio]").click( function() {

            var id  =   $(this).attr('id');

            switch(id) {
                case 'papeleta':
                    $('#box-multiple').addClass('hide');
                    $('#box-referendum').addClass('hide');
                    $('#box-' + id).removeClass('hide');
                break;

                case 'multiple':
                    $('#box-papeleta').addClass('hide');
                    $('#box-referendum').addClass('hide');
                    $('#box-' + id).removeClass('hide');
                break;

                case 'referendum':
                    $('#box-multiple').addClass('hide');
                    $('#box-papeleta').addClass('hide');
                    $('#box-' + id).removeClass('hide');
                break;
            }
        });

        $("#continuar_process").click(function(event){
            
            var type    =   $(".card-footer input[type=radio]:checked" ).val();
            var errores = false;

            if ('papeleta' == type) 
            {   

                $("#box-papeleta input").each(function( index ) {

                    var id = $(this).parents('.form-group').attr('id');

                    if ( 'optionTemplate' !== id ) {
                        
                        if ( $(this).val() == "" ) 
                        {
                            errores = true;
                            $(this).parents('.form-group').addClass('show_error');

                        }
                    } 
                  
                });


                if (errores == true)
                {
                    
                    $('#box-papeleta .alert-danger').remove();

                    $('#box-papeleta')
                        .append(
                            "<div class='alert alert-danger'>" +
                            "<strong>Error!</strong> No puede dejar opciones en blanco." +                            "</div>"
                            );

                    return false;
                }
            }

            else if ('referendum' == type) {

                var value  =   $("#box-referendum input").val();

                if (value == "") 
                {
                    errores = true;
                    $("#box-referendum").addClass('show_error');

                    $('#box-referendum .alert-danger').remove();

                    $('#box-referendum')
                        .append(
                            "<div class='alert alert-danger'>" +
                            "<strong>Error!</strong> No puede dejar el campo en blanco." +                            "</div>"
                            );

                    return false;                  
                }

            }

            else if ('multiple' == type) {//no sea cero
                
                $('#box-multiple .form-group').removeClass('show_error');
                $('#box-multiple input').removeClass('show_error');
                $('#box-multiple .alert-danger').remove();

                var numItems = $('#box-multiple input').length

                numItems    =   numItems - 3;
       
                $("#box-multiple input").each(function( index ) {

                    var id = $(this).parents('.form-group').attr('id');

                    if ( 'multipleoptionTemplate' !== id ) {
                        
                        if ( $(this).val() == "" ) 
                        {
                            errores = true;
                            $(this).parents('.form-group').addClass('show_error');

                        }
                    } 
                  
                });

                if (errores == true)
                {

                    $('#box-multiple')
                        .append(
                            "<div class='alert alert-danger'>" +
                            "<strong>Error!</strong> Por favor complete los campos faltantes." +                            
                            "</div>"
                            );

                }

                var min =   $('#box-multiple input[name=num_min]').val();

                var max =   $('#box-multiple input[name=num_max]').val();

                if (max !== "") {
                    if (max > numItems ) {
                        $('#box-multiple input[name=num_max]').addClass('show_error');
                        $('#box-multiple')
                                .append(
                                    "<div class='alert alert-danger'>" +
                                    "<strong>Error!</strong> La cantidad maxima no puede ser mayor al numero de items" +                   
                                    "</div>"
                                    );
                                errores = true;
                    }
               }

                if (min !== "") {

                     if (min <= 0){
                        $('#box-multiple input[name=num_min]').addClass('show_error');
                        $('#box-multiple')
                                .append(
                                    "<div class='alert alert-danger'>" +
                                    "<strong>Error!</strong> La cantidad minima debe ser superior a cero" +                    
                                    "</div>"
                                    );
                                errores = true;
                    }

                    if (min > numItems) {

                        $('#box-multiple input[name=num_min]').addClass('show_error');

                        $('#box-multiple')
                            .append(
                                "<div class='alert alert-danger'>" +
                                "<strong>Error!</strong> Verifique que la cantidad minima a seleccionar: " +  min + 
                                " no sea mayor a la cantidad de opciones: " + numItems +                    
                                "</div>"
                                );

                        errores = true;

                    }

                    if ( max !== "" && min > max ) {

                        $('#box-multiple input[name=num_min]').addClass('show_error');

                        $('#box-multiple')
                            .append(
                                "<div class='alert alert-danger'>" +
                                "<strong>Error!</strong> Verifique que la cantidad minima a seleccionar: " +  min +
                                " no sea mayor a la cantidad maxima: " + max +                  
                                "</div>"
                                );

                        errores = true;

                    }
                    

                }


                if (errores == true)
                {
                    return false;
                }
        
            }   

        });

    });
})(jQuery); 
