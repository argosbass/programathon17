App = {
    web3Provider: null,
    contracts: {},
    init: function () {
        return App.initWeb3();
    },
    initWeb3: function () {

        // Inicializa web3 y apunta el proveedor hacia testRPC.
        if (typeof web3 !== 'undefined')
        {
            App.web3Provider = web3.currentProvider;
            web3 = new Web3(web3.currentProvider);
        } else
        {
            // escoja el proveedor específico de Web3.providers
            App.web3Provider = new Web3.providers.HttpProvider('http://localhost:8545');
            web3 = new Web3(App.web3Provider);
        }

        return App.initContract();
    },
    initContract: function () {

        $.getJSON('build/contracts/Voting.json'
                , function (data)
                {
                    // Get the necessary contract artifact file and instantiate it with truffle-contract.
                    var AdoptionArtifact = data;

                    App.contracts.Voting = TruffleContract(AdoptionArtifact);

                    // Set the provider for our contract.
                    App.contracts.Voting.setProvider(App.web3Provider);


                });

        return App.bindEvents();
    },
    bindEvents: function () {
//        $(document).on('click', '#btn-crearProposal', App.handleCrearProposal);
//        $(document).on('click', '#btn-crearVote', App.handleCrearVoto);
        
//        $(document).on('click', '#btn-getResults', App.handleGetResults);

        App.handleCrearProposal();
        $(document).on('click', '#enviar-correos', App.handleCrearTokens);

        
    },
    handleCrearProposal: function () {


        var votingInstance;

        var f_inicial, f_final, publico, opciones;

        f_inicial  = $('#f_inicial_h').val();
        f_final    = $('#f_final_h').val();
        publico    = $('#publico').val();
        opciones   = $('#opciones').val();

        console.log("opciones",opciones);
        
        values = opciones.split(',');
        var opArr = [];

        $.each( values, function(key, line){  
                opArr.push(line.trim());
        });

        web3.eth.getAccounts(function (error, accounts)
        {
            if (error)
            {
                console.log(error);
            }

            var account = accounts[0];
            console.log("handleCrearProposal account", account);

            App.contracts.Voting.deployed().then(function (instance)
            {

                votingInstance = instance;

                votingInstance.newProposal( opArr , f_inicial, f_final, publico, {gas: 300000, from: account});

                votingInstance.getLastIdProposal.call().then(function (v) {

                    var id = v.toString();
                   console.log("LastIdProposal",id);

                   $('#LastIdProposal').val(id);

               });


            }).then(function (result)
            {

                console.log("handleCrearProposal result", result);

            }).catch(function (err) {

                console.log("handleCrearProposal ERROR", err.message);

            });
        });
    },


    handleCrearTokens: function (event) {


        console.log("handleCrearTokens");

        event.preventDefault();

        qtytokens  = $('#qty-tokens').val();
        qtytokens  = $('#qty-tokens').val();
                


        var votingInstance;

        web3.eth.getAccounts(function (error, accounts)
        {
            if (error)
            {
                console.log(error);
            }

            var account = accounts[0];
            console.log("handleCrearTokens account", account);

            App.contracts.Voting.deployed().then(function (instance)
            {

                votingInstance = instance;

                return votingInstance.newToken("0", "1", {gas: 300000, from: account});

            }).then(function (result)
            {
                console.log("handleCrearTokens result", result);
                return App.getTokens();

            }).catch(function (err) {

                console.log("handleCrearTokens ERROR", err.message);

            });
        });
    },
    getTokens: function () {


        console.log("getTokens");


        var votingInstance;

        web3.eth.getAccounts(function (error, accounts)
        {
            if (error)
            {
                console.log(error);
            }

            var account = accounts[0];
            console.log("getTokens account", account);

            App.contracts.Voting.deployed().then(function (instance)
            {

                votingInstance = instance;

                //  return votingInstance.getTokens( "0" , {gas: 300000, from: account});


                votingInstance.getTokens.call("0").then(function (v) {

                    console.log(v.toString());

                });


            })/*.then(function (result)
             {
             console.log("getTokens resulttoString()", result.toString());
             console.log("getTokens result", result);
             
             
             })*/.catch(function (err) {

                console.log("getTokens ERROR", err.message);

            });
        });


    },
   
    handleCrearVoto: function (event) {


        console.log("handleCrearProposal");

        event.preventDefault();

        var votingInstance;

        var userToken = $( "#voteToken" ).val();

        web3.eth.getAccounts(function (error, accounts)
        {
            if (error)
            {
                console.log(error);
            }

            var account = accounts[0];
            console.log("handleCrearProposal account", account);

            App.contracts.Voting.deployed().then(function (instance)
            {

                votingInstance = instance;
                

                return votingInstance.newVote("0","123","20170201", userToken,  {gas: 300000, from: account});

            }).then(function (result)
            {
                console.log("handleCrearProposal result", result);

            }).catch(function (err) {

                console.log("handleCrearProposal ERROR", err.message);

            });
        });
    },
    handleGetResults: function (event) {


        console.log("handleGetResults");

        event.preventDefault();

        var votingInstance;

        var option = $( "#optionResults" ).val();

        console.log("handleGetResults option", option);
        web3.eth.getAccounts(function (error, accounts)
        {
            if (error)
            {
                console.log(error);
            }

            var account = accounts[0];
            console.log("handleGetResults account", account);

            App.contracts.Voting.deployed().then(function (instance)
            {

                votingInstance = instance;

                return votingInstance.getResultsByOption("0","123",  {gas: 300000, from: account});

        /*        votingInstance.getResultsByOption.call("0",option).then(function (v) {

                    console.log(v.toString());

                    });
    */



            })
            .then(function (result)
            {
                console.log("handleCrearProposal result", result);

            }).catch(function (err) {

                console.log("handleCrearProposal ERROR", err.message);

            });
        });
    }
/*,
    
    handleGetResultsextra: function (event) {
         App.contracts.Voting.deployed().then(function (instance)
           {

               votingInstance = instance; 

               votingInstance.getLastIdProposal.call().then(function (v) {

                   console.log(v.toString());

               });


           })




             App.contracts.Voting.deployed().then(function (instance)
           {

               votingInstance = instance; 

               votingInstance.getResultsByOption.call("0","123").then(function (v) {

                   console.log(v.toString());

               });
           }
*/
};

$(function () {
    $(window).load(function () {
        App.init();
    });
});
