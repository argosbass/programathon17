(function($){
	$(document).ready(function(){
		//To render the input device to multiple email input using BootStrap icon
		$('#input_email').multiple_emails({position: "bottom"});
		
		$('#input_email').change( function(){
			$('.multiple_emails-close').text("Eliminar");
		});
		
		$('#upload-list-form').on('keyup keypress', function(e) {
		  var keyCode = e.keyCode || e.which;
		  if (keyCode === 13) { 
		    e.preventDefault();
		    return false;
		  }
		});
	});
})(jQuery); 
