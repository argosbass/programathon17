App = {
    web3Provider: null,
    contracts: {},
    init: function () {
        return App.initWeb3();
    },
    initWeb3: function () {

        // Inicializa web3 y apunta el proveedor hacia testRPC.
        if (typeof web3 !== 'undefined')
        {
            App.web3Provider = web3.currentProvider;
            web3 = new Web3(web3.currentProvider);
        } else
        {
            // escoja el proveedor específico de Web3.providers
            App.web3Provider = new Web3.providers.HttpProvider('http://localhost:8545');
            web3 = new Web3(App.web3Provider);
        }

        return App.initContract();
    },
    initContract: function () {

        $.getJSON('build/contracts/Voting.json'
                , function (data)
                {
                    // Get the necessary contract artifact file and instantiate it with truffle-contract.
                    var AdoptionArtifact = data;

                    App.contracts.Voting = TruffleContract(AdoptionArtifact);

                    // Set the provider for our contract.
                    App.contracts.Voting.setProvider(App.web3Provider);


                });

        return App.bindEvents();
    },
    bindEvents: function () {
        App.handleCrearProposal();
        
    },
    handleCrearProposal: function () {


        var votingInstance;

        var f_inicial, f_final, publico, opciones;

        f_inicial  = $('#f_inicial_h').val();
        f_final    = $('#f_final_h').val();
        publico    = $('#publico').val();
        opciones   = $('#opciones').val();

        console.log("opciones",opciones);
        
        values = opciones.split(',');
        var opArr = [];

        $.each( values, function(key, line){  
                opArr.push(line.trim());
        });

        web3.eth.getAccounts(function (error, accounts)
        {
            if (error)
            {
                console.log(error);
            }

            var account = accounts[0];
            console.log("handleCrearProposal account", account);

            App.contracts.Voting.deployed().then(function (instance)
            {

                votingInstance = instance;

                votingInstance.newProposal( opArr , f_inicial, f_final, publico, {gas: 300000, from: account});

                votingInstance.getLastIdProposal.call().then(function (v) {

                    var id = v.toString();
                   console.log("LastIdProposal",id);

                   $('#LastIdProposal').val(id);

               });


            }).then(function (result)
            {

                console.log("handleCrearProposal result", result);

            }).catch(function (err) {

                console.log("handleCrearProposal ERROR", err.message);

            });
        });
    }
    
};

$(function () {
    $(window).load(function () {
        App.init();
    });
});
