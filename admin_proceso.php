<?php 
include_once("seguridad.php");
include_once('vendor/adodb/adodb.inc.php');
include_once("vendor/config.php");

  if (session_status() == PHP_SESSION_NONE) 
  {
    session_start();
  }

  $nombre   = "";
  $fecha    = true;

  $f_actual = $f_inicial = $f_final = date('d/m/Y - H:i');

  if ( $_POST['f_inicial'] && $_POST['f_final'] ) 
  {

    $errores    = array();

    $f_inicial  = $_POST['f_inicial'];
    $f_final    = $_POST['f_final'];

    if ($f_inicial < $f_actual ) {
      
      $errores[]  = "La fecha inicial: " . $f_inicial . " no debe de ser menor a la fecha actual: " . $f_actual;
      $fecha    = false;
    }

    if ($f_inicial > $f_final ) {
      $errores[]  = "La fecha inicial: " . $f_inicial . " no debe de ser mayor a la fecha final: " . $f_final;
      $fecha    = false;
    }

    

  }

  if($_POST['continuar_process']!= "" && $_SESSION['process_id']!= "" && $fecha){

    $publico = $_POST['publico'];
    $avances = $_POST['avances'];
    $num_min = $_POST['num_min'];
    $num_max = $_POST['num_max'];

    if($_POST['publico']==""){
        $publico = 0;
    }
    if($_POST['avances']==""){
        $avances = 0;
    }
    if($_POST['num_min']==""){
        $num_min = 0;
    }
    if($_POST['num_max']==""){
        $num_max = 0;
    }

    $inicial = explode("-", $_POST['f_inicial']);
    $final = explode("-", $_POST['f_final']);

    $Finicial = explode("/", $inicial[0]);
    $Ffinal = explode("/", $final[0]);

    $Hinicial = explode(":", $inicial[1]);
    $Hfinal = explode(":", $final[1]);

    $fechaInicio = trim($Finicial[2])."-".trim($Finicial[1])."-".trim($Finicial[0])." ".trim($Hinicial[0]).":".trim($Hinicial[1]).":00";
    $fechaFinal  = trim($Ffinal[2])."-".trim($Ffinal[1])."-".trim($Ffinal[0])." ".trim($Hfinal[0]).":".trim($Hfinal[1]).":00";

    $sql = sprintf("UPDATE proceso_electoral SET fecha_inicio = '%s',fecha_final = '%s',tipo = '%s',voto_publico = %s,avances = %s,ether_id = 45,min = %s,max = %s where id = %s",$fechaInicio,$fechaFinal,$_POST['tipo'],$publico,$avances,$num_min,$num_max,$_SESSION['process_id']);
    $sql = $yoelijo->Prepare($sql);
    $RecordsetUpdate = $yoelijo->Execute($sql) or DIE($yoelijo->ErrorMsg());

    $pregunta = 0;
    if($_POST['tipo']=="papeleta"){
        $options = $_POST['papeleta_options'];
    }
    if($_POST['tipo']=="referendum"){
        $pregunta = 1;
        $options[0] = $_POST['refendum_option'];
    }
    if($_POST['tipo']=="multiple"){
        $options = $_POST['multiple_options'];
    }

    foreach ($options as $clave => $valor){
        if($valor!=""){
            $token = md5($_SESSION['process_id'].$valor.date('d/m/Y H:i:s'));
            $sql = sprintf("INSERT INTO options_proceso(id_proceso_electoral,pregunta,opcion,address) VALUES (%s,%s,'%s','%s')",$_SESSION['process_id'],$pregunta,$valor,$token);
            $sql = $yoelijo->Prepare($sql);
            $RecordsetInsert = $yoelijo->Execute($sql) or DIE($yoelijo->ErrorMsg());

            if ($pregunta) {
              
            $token = md5($_SESSION['process_id'].$valor.date('d/m/Y H:i:s').'NO');
            $sql = sprintf("INSERT INTO options_proceso(id_proceso_electoral,pregunta,opcion,address) VALUES (%s,%s,'%s','%s')",$_SESSION['process_id'],0,'SI',$token);
            $sql = $yoelijo->Prepare($sql);
            $RecordsetInsert = $yoelijo->Execute($sql) or DIE($yoelijo->ErrorMsg());

            $token = md5($_SESSION['process_id'].$valor.date('d/m/Y H:i:s').'SI');
            $sql = sprintf("INSERT INTO options_proceso(id_proceso_electoral,pregunta,opcion,address) VALUES (%s,%s,'%s','%s')",$_SESSION['process_id'],0,'NO',$token);
            $sql = $yoelijo->Prepare($sql);
            $RecordsetInsert = $yoelijo->Execute($sql) or DIE($yoelijo->ErrorMsg());

            }
        }
    }

    header('Location: agregar_emails.php'); 
       
  }
  
  if ($_SESSION['process_id'])
  {

    $query = "SELECT nombre FROM proceso_electoral WHERE ID = '%s'";
    $sql = sprintf($query, $_SESSION['process_id']);

    $sql = $yoelijo->Prepare($sql);

    $result = $yoelijo->Execute($sql) or DIE($yoelijo->ErrorMsg());

    if ($result->fields['nombre']) 
    {
      $nombre = $result->fields['nombre'];
    }

  }

?>

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Business Frontpage - Start Bootstrap Template</title>

    <!-- Bootstrap core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-datetimepicker.min.css" rel="stylesheet" media="screen">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">

    <!-- Custom styles for this template -->
    <link href="css/business-frontpage.css" rel="stylesheet">

    <!-- Temporary navbar container fix -->
    <style>
    .navbar-toggler {
        z-index: 1;
    }
    
	
    footer {
     /* position: absolute;
      bottom: 0;*/
      width: 100%; 
    }
    
   .admin-proceso {
     margin-top: 10%; 
   }

   .continuar {
      text-align: center;
      margin-top: 30px;
   }
   
   .menu-header {
     float:right;
   } 

   .form-inline label{
    display: inline;
   }
   	
   img.card-img-top.img-fluid {
    width: 100%;
    height: 200px;
   }

   .card-block{
    text-align: center;
   }

   .hide{
    display: none;
   }

   .form-group.show_error input ,
   #box-referendum.show_error input,
   #box-multiple input.show_error{
      border: solid 2px red;
   }



    @media (max-width: 576px) {
      .display-4{
        font-size: 200%;
      }
    }
 
    @media (max-width: 576px) {
        nav > .container {
            width: 100%;
        }
    }
    </style>

</head>

<body>

    <!-- Navigation -->
    <nav class="navbar fixed-top navbar-toggleable-md navbar-inverse bg-inverse">
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarExample" aria-controls="navbarExample" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="container">
            <a class="navbar-brand" href="#">Start Bootstrap</a>
            <div class="collapse navbar-collapse" id="navbarExample">
                <ul class="navbar-nav ml-auto">
                    <?php include_once("menu.php"); ?>
                </ul>
            </div>
        </div>
    </nav>

    <!-- Page Content -->
    <div class="container">
            <div class="col-sm-12">

              <?php 
              if ( !empty($errores)) 
              {
                foreach ($errores as $error) 
                {
                  echo "<br>";
                  echo "<div class='alert alert-danger'>";
                  echo "<strong>Error!</strong> " . $error;
                  echo "</div>";
                }

              }
              ?>


		<form class="form admin-proceso" role="form" method="post" action="admin_proceso.php">

    <div class="form-inline block-horas">
      <div class="form-group col-md-6">
        <label for="dtp_input1" class="col-md-12 control-label">Fecha y Hora de Inicio</label>
      
        <div class="input-group date form_datetime col-md-12" data-date-format="dd/mm/yyyy - hh:ii" data-link-field="dtp_input1">
            <input class="form-control" size="16" name="f_inicial" type="text" value="<?php echo $f_inicial;?>" readonly required>
            
            <span class="input-group-addon">
              <span class="fa fa-calendar"></span>
            </span>

        </div>  
        <input type="hidden" id="dtp_input1" value="" /><br/>
      </div>

      <div class="form-group col-md-6">
        <label for="dtp_input1" class="col-md-12 control-label">Fecha y Hora Final</label>
        
        <div class="input-group date form_datetime col-md-12" data-date-format="dd/mm/yyyy - hh:ii" data-link-field="dtp_input2">
            <input class="form-control" size="16" type="text" name="f_final" value="<?php echo $f_final;?>" readonly required>
            
            <span class="input-group-addon">
              <span class="fa fa-calendar"></span>
            </span>

        </div>  
        <input type="hidden" id="dtp_input2" value="" /><br/>
      </div>  		

    </div>

    <hr>


    <div class="row">
            <div class="col-sm-4 my-4">
                <div class="card">
                    <img class="card-img-top img-fluid" src="css/img/votacion.png" alt="">
                    <div class="card-block">
                        <h4 class="card-title">Papeleta</h4>
                    </div>
                    <div class="card-footer">
                        <input type="radio"  name="tipo" value="papeleta" class="form-control" id="papeleta" checked="checked"/>
                    </div>
                </div>
            </div>
            <div class="col-sm-4 my-4">
                <div class="card">
                    <img class="card-img-top img-fluid" src="css/img/referendum.jpg" alt="">
                    <div class="card-block">
                        <h4 class="card-title">Referendum</h4>
                    </div>
                    <div class="card-footer">
                        <input type="radio"  name="tipo" value="referendum"" class="form-control" id="referendum"/>
                    </div>
                </div>
            </div>
            <div class="col-sm-4 my-4">
                <div class="card">
                    <img class="card-img-top img-fluid" src="css/img/multiple.jpg" alt="">
                    <div class="card-block">
                        <h4 class="card-title">Opcion Multiple</h4>
                    </div>
                    <div class="card-footer">
                        <input type="radio"  name="tipo" value="multiple" class="form-control" id="multiple"/>
                    </div>
                </div>
            </div>

        </div>
        <!-- /.row -->

        <hr>

        <div id = "box-papeleta">
          <h3 class="control-label">Agregar Opciones para Papeleta</h3>

          <div class="form-group form-inline">
              <div class="col-xs-5">
                  <input type="text" class="form-control" name="papeleta_options[]"/>
              </div>
              <div class="col-xs-4">
                  <button type="button" class="btn btn-default addButton"><i class="fa fa-plus"></i></button>
              </div>
          </div>

        <!-- The option field template containing an option field and a Remove button -->
        <div class="form-group form-inline hide" id="optionTemplate">
            <div class="col-xs-offset-3 col-xs-5">
                <input class="form-control" type="text" name="papeleta_options[]"/>
            </div>
            <div class="col-xs-4">
                <button type="button" class="btn btn-default removeButton"><i class="fa fa-minus"></i></button>
            </div>
        </div>

      </div>

      <div id = "box-referendum" class="hide">

      <h3 class="control-label">Agregar una pregunta o propuesta para el Referendum</h3>
        <input type="textarea" class="col-md-12" name="refendum_option" style="height: 100px" />
      </div>


      <div id = "box-multiple" class="hide">

          <h3 class="control-label">Agregar Opciones</h3>

          <div class="form-group form-inline">
              <div class="col-xs-5 col-md-5"  style="padding-left:0px">
                  <input type="text" class="form-control" name="multiple_options[]"/>
              </div>
          </div>

          <div class="form-group form-inline">
              <div class="col-xs-5">
                  <input type="text" class="form-control" name="multiple_options[]"/>
              </div>
              <div class="col-xs-4">
                  <button type="button" class="btn btn-default addButtonmultiple"><i class="fa fa-plus"></i></button>
              </div>
          </div>

        <!-- The option field template containing an option field and a Remove button -->
        <div class="form-group form-inline hide" id="multipleoptionTemplate">
            <div class="col-xs-offset-3 col-xs-5">
                <input class="form-control" type="text" name="multiple_options[]"/>
            </div>
            <div class="col-xs-4">
                <button type="button" class="btn btn-default removeButton"><i class="fa fa-minus"></i></button>
            </div>
        </div>



          <div class="form-group col-md-6">
            <label for="dtp_input1" class="col-md-12 control-label">Opciones minimas a elegir</label>
            <div class="col-xs-5 col-md-5">
                <input type="number" class="form-control" name="num_min"/>
            </div>
          </div>

            <div class="form-group col-md-6">
              <label for="dtp_input1" class="col-md-12 control-label">Opciones maximas a elegir</label>
              <div class="col-xs-5 col-md-5">
                <input type="number" class="form-control" name="num_max"/>
              </div>
            </div>
          </div>

      <hr>

        <div class="form-inline check-options col-md-12">

          <div class="checkbox col-md-6 form-control">
            <label>
                <input type="checkbox" name="publico" value="1"/> Voto Publico
            </label>
          </div>

          <div class="checkbox col-md-6 form-control">
            <label>
              <input type="checkbox" name="avances" value="1"/> Avances
            </label>
          </div>

        </div>

			 <div class="form-group continuar">
    				<div class="col-sm-offset-2 col-sm-10">
                                    <input id="continuar_process" name="continuar_process" type="submit" class="btn btn-default" value="Continuar..."/>
    				</div>
  			</div>	
		</form>
        </div>
        <!-- /.row -->

    </div>
    <!-- /.container -->

    <!-- Footer -->
    <footer class="py-5 bg-inverse">
        <div class="container">
            <p class="m-0 text-center text-white">Copyright &copy; Your Website 2017</p>
        </div>
        <!-- /.container -->
    </footer>

    <!-- Bootstrap core JavaScript -->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/tether/tether.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/bootstrap-datetimepicker.js" charset="UTF-8"></script>
    <script type="text/javascript" src="js/locales/bootstrap-datetimepicker.fr.js" charset="UTF-8"></script>
    <script type='text/javascript' src="js/add_options.js"></script>
    <script type='text/javascript' src="js/programathon17.js"></script>
    <script type='text/javascript' src="js/multiple-emails.js"></script>
    <script src="js/web3.min.js"></script>
    <script src="js/truffle-contract.js"></script>
    <script src="js/app_voting.js"></script>
  
    <script type="text/javascript">
    $('.form_datetime').datetimepicker();
  $('.form_date').datetimepicker({
        language:  'fr',
        weekStart: 1,
        todayBtn:  1,
    autoclose: 1,
    todayHighlight: 1,
    startView: 2,
    minView: 2,
    forceParse: 0
    });
  $('.form_time').datetimepicker({
        language:  'fr',
        weekStart: 1,
        todayBtn:  1,
    autoclose: 1,
    todayHighlight: 1,
    startView: 1,
    minView: 0,
    maxView: 1,
    forceParse: 0
    });
  </script>

</body>

</html>
