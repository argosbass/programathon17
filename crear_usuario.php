<?php
function generaPass(){
    $cadena = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890";
    $longitudCadena=strlen($cadena);

    $pass = "";
    $longitudPass=8;

    for($i=1 ; $i<=$longitudPass ; $i++){
        $pos=rand(0,$longitudCadena-1);
        $pass .= substr($cadena,$pos,1);
    }
    return $pass;
}
include_once("seguridad.php");
include_once('vendor/adodb/adodb.inc.php');
include_once("vendor/config.php");
$error = 0;
//$yoelijo->debug=1;

if(isset($_POST['insert'])){
    if($_POST['nombre']!="" && $_POST['correo']!=""){
        $sql = sprintf("SELECT correo from usuarios where correo = '%s'",$_POST['correo']);
        $sql = $yoelijo->Prepare($sql);
        $RecordsetV	= $yoelijo->Execute($sql) or DIE($yoelijo->ErrorMsg());
        if($RecordsetV->EOF) {
            $tipo = $_POST['tipo'];
            if($tipo==""){
                $tipo = 2;
            }
            $nuevo = generaPass();
            $sql = sprintf("INSERT INTO usuarios (tipoUsuario,correo,clave,nombre)VALUES(%s,'%s','%s','%s')",$tipo,$_POST['correo'],md5($nuevo),$_POST['nombre']);
            $sql = $yoelijo->Prepare($sql);
            $RecordsetUpdate = $yoelijo->Execute($sql) or DIE($yoelijo->ErrorMsg());
            require("vendor/email/class.phpmailer.php");
            require("vendor/email/class.smtp.php");
            $mail = new PHPMailer();
            $mail->IsSMTP();
            $mail->SMTPDebug  = 0;
            $mail->Host       = 'smtp.gmail.com';
            $mail->Port       = 587;
            $mail->SMTPSecure = 'tls';
            $mail->SMTPAuth   = true;
            $mail->Username = 'getcode.cr@gmail.com';
            $mail->Password = 'getcode.123';
            $mail->From = 'getcode.cr@gmail.com';
            $mail->FromName = "YoElijo";
            $mail->AddAddress($_POST['correo']);
            $mail->Subject = "Nuevo Usuario";
            $mensajeC = "<table style=\"margin-left:auto;margin-right:auto;font-family:arial,sans-serif;background-color:#fff\" cellpadding=\"0\" cellspacing=\"0\" width=\"600px\"><tbody><tr><td style=\"padding:5px\"><table cellpadding=\"0\" cellspacing=\"0\" width=\"100%\"><tbody><tr><td width=\"25%\"><img src=\"\" class=\"CToWUd\"></td>";
            $mensajeC .= "<td colspan=\"2\" width=\"99%\"><h1 style=\"font-size:30px;font-weight:bold;color:#333;padding-top:20px;padding-left:1px;line-height:30px\">Nuevo Usuario de YoElijo</h1></td></tr></tbody></table><table width=\"100%\"><tbody><tr><td><table style=\"padding:12px;padding-top:0\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"570px\"><tbody>";
            $mensajeC .= "<tr><td colspan=\"2\"><p>Estimado(a): ".utf8_encode($_POST['nombre'])."</p>";
            $mensajeC .= "<p>Con el correo: <span style=\"font-weight:bold;\">".$_POST['correo']."</span></p>";
            $mensajeC .= "<p>Su nueva contraseña es <span style=\"font-weight:bold;\">$nuevo</span></p>";
            $mensajeC .= "<p>Muchas Gracias.<br>Enviado desde yoelijo.</p></td></tr>";
            $mensajeC .= "<tr><td align=\"center\" style=\"background-color:#fff\"><img src=\"\" align=\"absmiddle\" border=\"0\" height=\"60\" width=\"183\" class=\"CToWUd\"><div style=\"width:100%;color:#666666;font-size:10px\"><center>GetCode(). Todos los Derechos Reservados.</center></div></td></tr></tbody></table></td></tr></tbody></table></td></tr></tbody></table>";
            $mail->Body = $mensajeC;
            $mail->Send();
            header('Location: listaUsuarios.php?msj=3');
        }else{
            $error = 2;
        }
        
    }else{
        $error = 1;
    }     
}

if($_POST['id']!=""){
    $id = $_POST['id'];
}
if($_POST['tipo']!=""){
    $tipo = $_POST['tipo'];
}
if($_POST['nombre']!=""){
    $nombre = $_POST['nombre'];
}
if($_POST['correo']!=""){
    $correo = $_POST['correo'];
}

?><!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>getCode()</title>
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="css/business-frontpage.css" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
    <style>
    .navbar-toggler {
        z-index: 1;
    }
    
    @media (max-width: 576px) {
        nav > .container {
            width: 100%;
        }
    }
    </style>
    <script>
        function confirmar(id){
            var r = confirm("Esta seguro que desea eliminar el usuario "+id);
            if (r == true) {
                window.location = "eliminarUsuarios.php?id="+id;
            }
        }
        </script>

</head>

<body>

    <!-- Navigation -->
    <nav class="navbar fixed-top navbar-toggleable-md navbar-inverse bg-inverse">
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarExample" aria-controls="navbarExample" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="container">
            <a class="navbar-brand" href="#">Start Bootstrap</a>
            <div class="collapse navbar-collapse" id="navbarExample">
                <ul class="navbar-nav ml-auto">
                    <?php include_once("menu.php"); ?>
                </ul>
            </div>
        </div>
    </nav>

    <!-- Page Content -->
    <div class="container">
        <h1>Crear Usuario</h1>
        <br>
        <?php if($error==1){ ?>
        <div class="alert alert-danger">
          <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
          <strong>Validaci&oacute;n!</strong> Todos los campos son requeridos.
        </div>
        <?php } ?>
        <?php if($error==2){ ?>
        <div class="alert alert-danger">
          <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
          <strong>Validaci&oacute;n!</strong> Correo ya existe en la base de datos.
        </div>
        <?php } ?>
      <form id="upload-list-form" role="form" method="post" action="">
          <input type="hidden" name="id" value="<?php echo $id; ?>">
        <div class="form-group">
            <input type="email" name="correo" placeholder="Correo" class="form-control" value="<?php echo $correo; ?>">
        </div>
        <div class="form-group">
            <input type="text" name="nombre" placeholder="Nombre Usuario" class="form-control" value="<?php echo $nombre; ?>">
        </div>
        <div class="form-group">
          <div class="checkbox">
            <label>
                <input type="checkbox" name="tipo" value="1" <?php if($tipo==1){ ?>checked<?php } ?>> Este usuario es administrador?
            </label>
          </div>      
        </div>
          <input type="submit" name="insert" id="editar" class="btn btn-success" value="Crear">
          <a href="./listaUsuarios.php" class="btn btn-danger" role="button">Cancelar</a>
          <br>
    </form>


    </div>

    <!-- Footer -->
    <footer class="py-5 bg-inverse">
        <div class="container">
            <p class="m-0 text-center text-white">Copyright &copy; Your Website 2017</p>
        </div>
        <!-- /.container -->
    </footer>

    <!-- Bootstrap core JavaScript -->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/tether/tether.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.min.js"></script>

</body>

</html>
