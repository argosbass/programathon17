App = {
    web3Provider: null,
    contracts: {},
    init: function () {
        return App.initWeb3();
    },
    initWeb3: function () {
        
        // Inicializa web3 y apunta el proveedor hacia testRPC.
        if (typeof web3 !== 'undefined')
        {
            App.web3Provider = web3.currentProvider;
            web3 = new Web3(web3.currentProvider);
        } else
        {
            // escoja el proveedor específico de Web3.providers
            App.web3Provider = new Web3.providers.HttpProvider('http://localhost:8545');
            web3 = new Web3(App.web3Provider);
        }

        return App.initContract();
    },
    initContract: function () {
        
        $.getJSON('Adoption.json'
                , function (data)
                {
                    // Get the necessary contract artifact file and instantiate it with truffle-contract.
                    var AdoptionArtifact = data;

                    App.contracts.Adoption = TruffleContract(AdoptionArtifact);

                    // Set the provider for our contract.
                    App.contracts.Adoption.setProvider(App.web3Provider);

                    // Use our contract to retieve and mark the adopted pets.
                    return App.getAdopted();
                });

        return App.bindEvents();
    },
    bindEvents: function () {
        $(document).on('click', '.btn-adopt', App.handleAdopt);

        $(document).on('click', '#btn-crearDinero', App.handleCrearDinero);
        
        $(document).on('click', '#btn-transDinero', App.handleTransferirDinero);
 
        $(document).on('click', '#btn-getBalance', App.handleGetBalance);
        
    },
    handleCrearDinero: function (event) {

            
        console.log("handleCrearDinero");
        
        event.preventDefault();

        var adoptionInstance;

        web3.eth.getAccounts(function (error, accounts)
        {
            if (error)
            {
                console.log(error);
            }

            var account = accounts[0];
            console.log("handleCrearDinero account", account);

            App.contracts.Adoption.deployed().then(function (instance)
            {

                console.log("handleCrearDinero instance", instance);
                adoptionInstance = instance;

                var destino = $("#txt-crearDinero-cuenta").val();
                var monto = $("#txt-crearDinero-monto").val();
                
                console.log("handleCrearDinero destino", destino);
                console.log("handleCrearDinero monto", monto);
                
                return adoptionInstance.crearDinero(destino, monto);

            }).then(function (result)
            {
                console.log("handleCrearDinero result", result);

            }).catch(function (err) {

                console.log("handleCrearDinero ERROR", err.message);

            });
        });
    },
    handleTransferirDinero: function (event) {

        console.log("handleTransferirDinero");
        
        event.preventDefault();

        var adoptionInstance;

// WORKS 
/*        var origen = $("#txt-transDinero-cuentaOrigen").val();
        var destino = $("#txt-transDinero-cuentaDestino").val();
        var monto = $("#txt-transDinero-monto").val();

      web3.eth.sendTransaction({
        from: origen,
        to: destino,
        value: web3.toWei( monto , 'ether')
      }, function(error, result) {
        if (!error) {
        
        console.log("handleTransferirDinero result", result);

//         $("#div-getBalance-result").append("<p>Balance for address("+cuenta+"): "+balance+"</p>");
        
        } else {
        
                console.log("handleTransferirDinero error", error);
        
        }
      })
*/    
// WORKS




        web3.eth.getAccounts(function (error, accounts)
        {
            if (error)
            {
                console.log(error);
            }

            var account = accounts[0];
            console.log("handleTransferirDinero account", account);


            App.contracts.Adoption.deployed().then(function (instance)
            {

                console.log("handleTransferirDinero instance", instance);
                adoptionInstance = instance;

                var origen = $("#txt-transDinero-cuentaOrigen").val();
                var destino = $("#txt-transDinero-cuentaDestino").val();
                var monto = $("#txt-transDinero-monto").val();
                
                console.log("handleTransferirDinero origen", origen);
                console.log("handleTransferirDinero destino", destino);
                console.log("handleTransferirDinero monto", monto);
                
                return adoptionInstance.transferir( {from: origen }, destino, web3.toWei( monto , 'ether'));

            }).then(function (result)
            {
                console.log("handleTransferirDinero result", result);

            }).catch(function (err) {

                console.log("handleTransferirDinero ERROR", err.message);

            });
        });
    },
    handleGetBalance: function (event) {

            
        console.log("handleGetBalance");
        
        event.preventDefault();

        var adoptionInstance;

                var cuenta = $("#txt-obtenerBalance-cuentaOrigen").val();
                
                
                console.log("handleGetBalance cuenta", cuenta);
                
        web3.eth.getBalance(cuenta, 'latest', function(error, result){
            
            if(!error){
                
            
                var balance = Number(web3.fromWei(result, "ether"));
                
                console.log("handleGetBalance result"+cuenta, result);

                $("#div-getBalance-result").append("<p>Balance for address("+cuenta+"): "+balance+"</p>");
            
            }
            else
            {
            console.log("handleGetBalance error", error);
            }
        })






/*
 * return new Promise (function (resolve, reject) {
    web3.eth.getBalance(address, function (error, result) {
      if (error) {
        reject(error);
      } else {
        resolve(result);
    }
  })
 * 
 * 
        web3.eth.getAccounts(function (error, accounts)
        {
            if (error)
            {
                console.log(error);
            }

            var account = accounts[0];
            console.log("handleGetBalance account", account);

            App.contracts.Adoption.deployed().then(function (instance)
            {

                console.log("handleGetBalance instance", instance);
                adoptionInstance = instance;

                var cuenta = $("#txt-obtenerBalance-cuentaOrigen").val();
                
                
                console.log("handleGetBalance cuenta", cuenta);
                
                w3web.
                return adoptionInstance.getBalance(cuenta);

            }).then(function (result)
            {
                console.log("handleGetBalance result", result);

            }).catch(function (err) {

                console.log("handleGetBalance ERROR", err.message);

            });
        }); */
    },
    handleAdopt: function (event) {

        event.preventDefault();

        var petId = parseInt($(event.target).data('id'));


        /*
         * Replace me...
         */

        var adoptionInstance;

        web3.eth.getAccounts(function (error, accounts)
        {
            if (error)
            {
                console.log(error);
            }

            var account = accounts[0];

            console.log("accounts", accounts);
            console.log("account", account);

            App.contracts.Adoption.deployed().then(function (instance)
            {
                alert("instance");
                adoptionInstance = instance;

                return adoptionInstance.adopt(petId, {from: account});

            }).then(function (result)
            {
                alert("result");
                return App.markAdopted();

            }).catch(function (err) {

                alert("message");
                alert(err);
                alert(err.message);
                console.log(err.message);

            });
        });
    },
    getAdopted: function (adopters, account) {
              
        var adoptionInstance;

        App.contracts.Adoption.deployed().then(function (instance)
        {
            adoptionInstance = instance;

            return adoptionInstance.getAdopters.call();
        
        }).then(function (adopters)
        {
            console.log("adopters", adopters);

            for (i = 0; i < adopters.length; i++)
            {
                $('#accountsdataRow').append("<h5>" + i + " : " + adopters[i] + "</h5>");
            }

        }).catch(function (err) {
            console.log(err.message);
        });
    }
};

$(function () {
    $(window).load(function () {
        App.init();
    });
});
