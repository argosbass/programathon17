<?php 
include_once("seguridad.php");
  ini_set('display_errors', 'On');
  if (session_status() == PHP_SESSION_NONE) 
  {
    session_start();
  }

  if ( isset($_SESSION['getEther_id'])  &&  ( $_SESSION['getEther_id'] != "")  )
  {
    $_SESSION['getEther_id'] = "";
  }

  if (!isset( $_SESSION['process_id'] ) )
  {
    
    if ( isset($_POST['nombre_proceso']) )
    {

      include_once('vendor/adodb/adodb.inc.php');
      include_once("vendor/config.php");    

      $id_user  = "";

      if ($_SESSION['id']) {
        $id_user  = $_SESSION['id'];
      }

      $query  = "INSERT INTO proceso_electoral (nombre,status, id_admin) VALUES ('%s', '%s', %d)";
      $sql = sprintf($query, $_POST["nombre_proceso"], "creando", (int)$id_user);
      
      $sql = $yoelijo->Prepare($sql);

      $Recordset  = $yoelijo->Execute($sql) or DIE($yoelijo->ErrorMsg());

      if($Recordset->EOF) 
      {
        $get_id = "SELECT ID FROM proceso_electoral ORDER BY ID DESC LIMIT 1";

        $sql    = $yoelijo->Prepare($get_id);
        $result = $yoelijo->Execute($sql) or DIE($yoelijo->ErrorMsg());

        $process_id = $result->fields['ID'];
        
        $_SESSION['process_id'] = $process_id;
        
        header('Location: admin_proceso.php');
      }

    }

  }else{
    header('Location: admin_proceso.php');
  }


?>


<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>getCode()</title>

    <!-- Bootstrap core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/business-frontpage.css" rel="stylesheet">

    <!-- Temporary navbar container fix -->
    <style>
    .navbar-toggler {
        z-index: 1;
    }
    
	
    footer {
      position: absolute;
      bottom: 0;
      width: 100%; 
    }
    
   .agregar-nombre {
     margin-top: 10%; 
   }

   .continuar {
      text-align: center;
   }
   
   .menu-header {
     float:right;
   } 
   	
    @media (max-width: 576px) {
      .display-4{
        font-size: 200%;
      }
    }
 
    @media (max-width: 576px) {
        nav > .container {
            width: 100%;
        }
    }
    </style>

</head>

<body>

    <!-- Navigation -->
    <nav class="navbar fixed-top navbar-toggleable-md navbar-inverse bg-inverse">
        <div class="container">
            <a class="navbar-brand" href="#">yoelijo</a>
            
                <ul class="navbar-nav ml-auto col-ms-2 menu-header">
                    <?php include_once("menu.php"); ?>
                </ul>
            </div>
        </div>
    </nav>

    <!-- Header with Background Image -->
    <header class="business-header">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="title display-4 text-white mt-4">Nombre del Proceso Electoral</h1>
                </div>
            </div>
        </div>
    </header>

    <!-- Page Content -->
    <div class="container">
            <div class="col-sm-12">
		<form class="form agregar-nombre" role="form" method="post" action="nombre_proceso.php">
  			<div class="form-group">
    				<input type="text" class="form-control" id="nombre-proceso" name="nombre_proceso" placeholder="Nombre Proceso" required>
  			</div>
  			
			 <div class="form-group continuar">
    				<div class="col-sm-offset-2 col-sm-10">
      					<button type="submit" class="btn btn-default">Continuar...</button>
    				</div>
  			</div>	
		</form>
        </div>
        <!-- /.row -->

    </div>
    <!-- /.container -->

    <!-- Footer -->
    <footer class="py-5 bg-inverse">
        <div class="container">
            <p class="m-0 text-center text-white">Copyright &copy; Your Website 2017</p>
        </div>
        <!-- /.container -->
    </footer>

    <!-- Bootstrap core JavaScript -->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/tether/tether.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.min.js"></script>

</body>

</html>
