<?php
    $error = 0;
    if(isset($_POST['login'])){
        //$yoelijo->debug = 1;
        
        if(isset($_POST['correo']) && isset($_POST['clave'])){
            include_once('vendor/adodb/adodb.inc.php');
            include_once("vendor/config.php");
            require_once("vendor/recaptchalib.php");
            //recaptcha
            $secret = "6LddxjEUAAAAADQYhC3FU-k-iZGt1sfT6ZnI8fzJ";
            $response = null;
            $reCaptcha = new ReCaptcha($secret);
            if ($_POST["g-recaptcha-response"]) {
                $response = $reCaptcha->verifyResponse($_SERVER["REMOTE_ADDR"],$_POST["g-recaptcha-response"]);
                if ($response != null && $response->success) {
                    $sql = sprintf("SELECT id,clave,tipoUsuario,nombre from usuarios where correo = '%s'",$_POST['correo']);
                    $sql = $yoelijo->Prepare($sql);
                    $Recordset	= $yoelijo->Execute($sql) or DIE($yoelijo->ErrorMsg());
                    if(!$Recordset->EOF) {
                        //echo $Recordset->Fields("clave") . "==".md5($_POST['clave'])."<br>";
                        if($Recordset->Fields("clave") == md5($_POST['clave'])){
                            session_start();
                            $_SESSION['id'] = $Recordset->Fields("id");
                            $_SESSION['correo'] = $_POST['correo'];
                            $_SESSION['tipoUsuario'] = $Recordset->Fields("tipoUsuario");
                            $_SESSION['nombre'] = $Recordset->Fields("nombre");
                            header('Location: index.php');
                        }else{
                            $error = 2;
                        }
                    }else{
                        $error = 2;
                    }
                }else{
                    $error = 1;
                }
            }else{
                $error = 1;
            }
        }else{
            $error = 1;
        }
    }	
?><!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>getCode()</title>
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
	<style>
		@import url(http://fonts.googleapis.com/css?family=Roboto);

		/****** LOGIN MODAL ******/
		.loginmodal-container {
		  padding: 30px;
		  max-width: 350px;
		  width: 100% !important;
		  background-color: #F7F7F7;
		  margin: 0 auto;
		  border-radius: 2px;
		  box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.3);
		  overflow: hidden;
		  font-family: roboto;
		}

		.loginmodal-container h1 {
		  text-align: center;
		  font-size: 1.8em;
		  font-family: roboto;
		}

		.loginmodal-container input[type=submit] {
		  width: 100%;
		  display: block;
		  margin-bottom: 10px;
		  position: relative;
		}

		.loginmodal-container input[type=text], input[type=password] {
		  height: 44px;
		  font-size: 16px;
		  width: 100%;
		  margin-bottom: 10px;
		  -webkit-appearance: none;
		  background: #fff;
		  border: 1px solid #d9d9d9;
		  border-top: 1px solid #c0c0c0;
		  /* border-radius: 2px; */
		  padding: 0 8px;
		  box-sizing: border-box;
		  -moz-box-sizing: border-box;
		}

		.loginmodal-container input[type=text]:hover, input[type=password]:hover {
		  border: 1px solid #b9b9b9;
		  border-top: 1px solid #a0a0a0;
		  -moz-box-shadow: inset 0 1px 2px rgba(0,0,0,0.1);
		  -webkit-box-shadow: inset 0 1px 2px rgba(0,0,0,0.1);
		  box-shadow: inset 0 1px 2px rgba(0,0,0,0.1);
		}

		.loginmodal {
		  text-align: center;
		  font-size: 14px;
		  font-family: 'Arial', sans-serif;
		  font-weight: 700;
		  height: 36px;
		  padding: 0 8px;
		/* border-radius: 3px; */
		/* -webkit-user-select: none;
		  user-select: none; */
		}

		.loginmodal-submit {
		  /* border: 1px solid #3079ed; */
		  border: 0px;
		  color: #fff;
		  text-shadow: 0 1px rgba(0,0,0,0.1); 
		  background-color: #4d90fe;
		  padding: 17px 0px;
		  font-family: roboto;
		  font-size: 14px;
		  /* background-image: -webkit-gradient(linear, 0 0, 0 100%,   from(#4d90fe), to(#4787ed)); */
		}

		.loginmodal-submit:hover {
		  /* border: 1px solid #2f5bb7; */
		  border: 0px;
		  text-shadow: 0 1px rgba(0,0,0,0.3);
		  background-color: #357ae8;
		  /* background-image: -webkit-gradient(linear, 0 0, 0 100%,   from(#4d90fe), to(#357ae8)); */
		}

		.loginmodal-container a {
		  text-decoration: none;
		  color: #666;
		  font-weight: 400;
		  text-align: center;
		  display: inline-block;
		  opacity: 0.6;
		  transition: opacity ease 0.5s;
		} 

		.login-help{
		  font-size: 12px;
		}
		
		.loginmodal-by {
			max-width: 350px;
			width: 100% !important;
			margin: 0 auto;
			overflow: hidden;
			font-family: roboto;
			text-align: center;
			color: #666;
		}
	</style>
</head>
<body>
    <div class="container">
	  <div class="modal-dialog">
			<div class="loginmodal-container">
			  <h1>yoelijo</h1><br>
                          <?php if($error==1){ ?>
                          <div class="alert alert-danger">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
                            <strong>Validaci&oacute;n!</strong> Todos los campos son requeridos.
                          </div>
                          <?php } ?>
                          <?php if($error==2){ ?>
                          <div class="alert alert-danger">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
                            <strong>Validaci&oacute;n!</strong> EL correo o contrase&ntilde;a es invalido.
                          </div>
                          <?php } ?>
                          <form id="register-form" role="form" method="post" action="">
				<input type="text" name="correo" placeholder="Correo">
				<input type="password" name="clave" placeholder="Contrase&ntilde;a">
                                <div class="g-recaptcha" data-sitekey="6LddxjEUAAAAALfSIiYBSNwu9PG1Yo1wZGMGaIX3"></div>
				<input type="submit" name="login" id="login" class="login loginmodal-submit" value="Ingresar">
			  </form>	
			  <div class="login-help">
				<a href="olvido.php">Se te olvid&oacute; tu contrase&ntilde;a</a>
			  </div>
			</div>
			<div class="loginmodal-by">Creado por getCode()</div>
		</div>
	  </div>
    </div>
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/tether/tether.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.min.js"></script>
    <script src='https://www.google.com/recaptcha/api.js?hl=es'></script>
</body>

</html>
