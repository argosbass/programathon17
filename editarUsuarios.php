<?php
include_once("seguridad.php");
include_once('vendor/adodb/adodb.inc.php');
include_once("vendor/config.php");
$error = 0;
if($_GET['id']!=""){
    include_once('vendor/adodb/adodb.inc.php');
    include_once("vendor/config.php");
    //$yoelijo->debug=1;
 
    if(isset($_POST['editar'])){
        if($_POST['nombre']!="" && $_POST['correo']!=""){
            $sql = sprintf("SELECT correo from usuarios where correo = '%s' and id != %s",$_POST['correo'],$_POST['id']);
            $sql = $yoelijo->Prepare($sql);
            $RecordsetV	= $yoelijo->Execute($sql) or DIE($yoelijo->ErrorMsg());
            if($RecordsetV->EOF) {
                $tipo = $_POST['tipo'];
                if($tipo==""){
                    $tipo = 2;
                }
                $sql = sprintf("UPDATE usuarios SET nombre = '%s',correo = '%s',tipoUsuario = %s where id = %s",$_POST['nombre'],$_POST['correo'],$tipo,$_POST['id']);
                $sql = $yoelijo->Prepare($sql);
                $RecordsetUpdate = $yoelijo->Execute($sql) or DIE($yoelijo->ErrorMsg());
                header('Location: listaUsuarios.php?msj=2');
            }else{
                $error = 2;
            }
        }else{
            $error = 1;
        }     
    }
    $sql = sprintf("SELECT id,clave,tipoUsuario,nombre,correo from usuarios where id = '%s'",$_GET['id']);
    $sql = $yoelijo->Prepare($sql);
    $Recordset	= $yoelijo->Execute($sql) or DIE($yoelijo->ErrorMsg());
    
    if($_POST['id']!=""){
        $id = $_POST['id'];
    }else{
        $id = $Recordset->Fields("id");
    }
    if($_POST['tipo']!=""){
        $tipo = $_POST['tipo'];
    }else{
        $tipo = $Recordset->Fields("tipoUsuario");
    }
    if($_POST['nombre']!=""){
        $nombre = $_POST['nombre'];
    }else{
        $nombre = $Recordset->Fields("nombre");
    }
    if($_POST['correo']!=""){
        $correo = $_POST['correo'];
    }else{
        $correo = $Recordset->Fields("correo");
    }  
}else{
    header('Location: index.php');
}
?><!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>getCode()</title>
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="css/business-frontpage.css" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
    <style>
    .navbar-toggler {
        z-index: 1;
    }
    
    @media (max-width: 576px) {
        nav > .container {
            width: 100%;
        }
    }
    </style>
    <script>
        function confirmar(id){
            var r = confirm("Esta seguro que desea eliminar el usuario "+id);
            if (r == true) {
                window.location = "eliminarUsuarios.php?id="+id;
            }
        }
        </script>

</head>

<body>

    <!-- Navigation -->
    <nav class="navbar fixed-top navbar-toggleable-md navbar-inverse bg-inverse">
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarExample" aria-controls="navbarExample" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="container">
            <a class="navbar-brand" href="#">Start Bootstrap</a>
            <div class="collapse navbar-collapse" id="navbarExample">
                <ul class="navbar-nav ml-auto">
                <?php include_once("menu.php"); ?>
                </ul>
            </div>
        </div>
    </nav>

    <!-- Page Content -->
    <div class="container">
        <h1>Editar Usuario</h1>
        <br>
        <?php if($error==1){ ?>
        <div class="alert alert-danger">
          <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
          <strong>Validaci&oacute;n!</strong> Todos los campos son requeridos.
        </div>
        <?php } ?>
        <?php if($error==2){ ?>
        <div class="alert alert-danger">
          <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
          <strong>Validaci&oacute;n!</strong> Correo ya existe en la base de datos.
        </div>
        <?php } ?>
      <form id="upload-list-form" role="form" method="post" action="">
          <input type="hidden" name="id" value="<?php echo $id; ?>">
        <div class="form-group">
            <input type="email" name="correo" placeholder="Correo" class="form-control" value="<?php echo $correo; ?>">
        </div>
        <div class="form-group">
            <input type="text" name="nombre" placeholder="Nombre Usuario" class="form-control" value="<?php echo $nombre; ?>">
        </div>
        <div class="form-group">
          <div class="checkbox">
            <label>
                <input type="checkbox" name="tipo" value="1" <?php if($tipo==1){ ?>checked<?php } ?>> Este usuario es administrador?
            </label>
          </div>      
        </div>
          <input type="submit" name="editar" id="editar" class="btn btn-success" value="Guardar">
          <a href="./listaUsuarios.php" class="btn btn-danger" role="button">Cancelar</a>
          <br>
    </form>


    </div>

    <!-- Footer -->
    <footer class="py-5 bg-inverse">
        <div class="container">
            <p class="m-0 text-center text-white">Copyright &copy; Your Website 2017</p>
        </div>
        <!-- /.container -->
    </footer>

    <!-- Bootstrap core JavaScript -->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/tether/tether.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.min.js"></script>

</body>

</html>
