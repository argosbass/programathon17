<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Pete's Pet Shop</title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
    <div class="container">
      <div class="row">
        <div class="col-xs-12 col-sm-8 col-sm-push-2">
          <h1 class="text-center">Pete's Pet Shop</h1>
          
          <br/>
        </div>
      </div>

      <div id="accountsdataRow" class="row">
        
      </div>
  <hr/>
      <div class="row">
        <div class="col-xs-12 col-sm-8 col-sm-push-2">
          <h1 class="text-center">CREAR Proposal</h1>
          
          <br/>
          <button id="btn-crearProposal" > CREAR Proposal </button>
        </div>
      </div>
<hr/>
      <div class="row">
        <div class="col-xs-12 col-sm-8 col-sm-push-2">
          <h1 class="text-center">CREAR Tokens</h1>
          
          <br/>
          <button id="btn-crearTokens" > CREAR Tokens </button>
          
          <h3  > Creados Tokens </h3>

          <div id="div-createdTokens" >  </div>          
        </div>
      </div>
      
<hr/>
      <div class="row">
        <div class="col-xs-12 col-sm-8 col-sm-push-2">
          <h1 class="text-center">CREAR voto</h1>
          
          <br/>
          <button id="btn-crearVote" > CREAR voto </button>
          
          <h3  > Creados Tokens </h3>

          <input id="voteToken" value="" >  </input>
        </div>
      </div>
        
<hr/>
      <div class="row">
        <div class="col-xs-12 col-sm-8 col-sm-push-2">
          <h1 class="text-center">Obtener por opcion</h1>
          
          <br/>
          <button id="btn-getResults" > Obtener por opcion </button>
          
         

          <input id="optionResults" value="" >  </input>
        </div>
      </div>


    </div>

    

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
    <script src="js/web3.min.js"></script>
    <script src="js/truffle-contract.js"></script>
    <script src="js/app_voting.js"></script>
  </body>
</html>
