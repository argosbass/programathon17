-- MySQL dump 10.13  Distrib 5.7.12, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: yoelijo
-- ------------------------------------------------------
-- Server version	5.5.5-10.1.19-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `invitaciones`
--

DROP TABLE IF EXISTS `invitaciones`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `invitaciones` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_proceso_electoral` int(11) DEFAULT NULL,
  `correo` varchar(100) DEFAULT NULL,
  `token` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_invitaciones_ref_proceso_electoral_idx` (`id_proceso_electoral`),
  CONSTRAINT `fk_invitaciones_ref_proceso_electoral` FOREIGN KEY (`id_proceso_electoral`) REFERENCES `proceso_electoral` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=146 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `invitaciones`
--

LOCK TABLES `invitaciones` WRITE;
/*!40000 ALTER TABLE `invitaciones` DISABLE KEYS */;
INSERT INTO `invitaciones` VALUES (13,2,'cris5580@gmail.com','f8158d700b0a450c1441b808e1caab47'),(14,2,'cris55801@gmail.com','f8158d790b0a450c1441b808e1caab47'),(15,2,'cris55802@gmail.com','f8219d700b0a450c1441b808e1caab47'),(16,5,'programathon1@gmail.com','94d6afcd8575909c713492403d5350e61506254106'),(17,5,'programathon2@gmail.com','d2a7733a73434fafd666adf68515993c1506254108'),(32,10,'pab.quiros@gmail.com','825a4660981eb4e5a149d33e9fae3b191506257725'),(33,11,'pab.quiros@gmail.com','eee13ae3420ac5bef159475c5cd749841506258465'),(34,12,'cris5580@gmail.com','ac6b3bdebb282bbc40fee6137461121b1506259362'),(35,13,'cris5580@gmai.com','6d319dfae3a8c08eeb9699cc5cba6a4a1506259983'),(36,14,'cris5580@gmail.com','efbef1d5f345349d7bb77c5b5ef060371506260255'),(37,15,'programathon2@gmail.com','58514113e5cbca9458f94a2969e461c71506261631'),(38,15,'programathon3@gmail.com','73b5d8738500e0e1d5930f4c5bf9b42e1506261633'),(39,15,'cris5580@gmail.com','5a1f356dd48e39414a422a5eaeace2991506261635'),(40,16,'u1@test.com','a6521dba2fe5f1bc7dfa301787e3a3641506261992'),(41,16,'u2@test.com','7528e4b75ae185498e9f273291d9a9ec1506261994'),(42,16,'u3@test.com','06ae2ef151bfbba3b6ce209526fc184e1506261995'),(43,16,'u4@test.com','5ec3611d4021a7ddbd840aeaa902c05d1506261996'),(44,16,'u5@test.com','69612c75c6b82dcbe852180f2c120d031506261998'),(45,16,'programathon2@gmail.com\r\n','a10c708cb260ae8febbbe443fac1ce0f1506261999'),(60,19,'u1@test.com','370dec080707f724758cc7edc4bf7b251506262743'),(61,19,'u2@test.com','cb5d0a077e8e808644e52eddcaec6b901506262745'),(62,19,'u3@test.com','ffa08bbf5aa63ba2008c1f2ccf75ff741506262746'),(63,19,'u4@test.com','cbcab8f0a91955ee9384e61b1e02dd9e1506262747'),(64,19,'u5@test.com','e830ab2bbec7b644c7f9b60df265823b1506262749'),(65,19,'programathon2@gmail.com','f3f3d74026c5a8c3c620fb1b7a4215621506262750'),(66,19,'cris5580@gmail.com\r\n','8b64e24a5d8a3baf9d10ab06ce9e23f51506262752'),(67,20,'programathon1@gmail.com','2bbd1c0cd8b4c7bb5ec18f1b58b8778a1506263575'),(68,21,'pab.quiros@gmail.com','ea536d1b5d973bea3d3c6e83cf42da281506264561'),(71,22,'pab.quiros@gmail.com','e18a24f103ff1ae0be217d74f28809721506265144'),(72,23,'jupmasalamanca@gmail.com','6a602dbdc697b179a57519230357c05d1506265569'),(73,23,'cris5580@gmail.com','7b0a451791a389c68347d47f6fce5f3e1506265571'),(74,24,'programathon2@gmail.com','2eeb6feabe6f145bd2fcdb334738e9e31506265958'),(75,24,'cris5580@gmail.com','065302fae990f8a7686df69cce35c6ac1506265961'),(76,24,'jupmasalamanca@gmail.com','2f3e49d3a2d2de496418546542c5e16c1506265962'),(77,25,'cris5580@gmail.com','65aaeab4b553efa88962ffd9046f6dc71506267362'),(78,26,'cris5580@gmail.com','d0e4c4144e47a499b1d205a80a5595401506267459'),(79,26,'programathon2@gmail.com','779efc77d488ce6db4e40e7db9150c671506267462'),(80,27,'programathon2@gmail.com','9e15701ab4b705fb7e6a07e87abdd7721506268073'),(81,27,'cris5580@gmail.com','394c4c88d78fc4587c2d861f634b6d971506268075'),(82,28,'programathon2@gmail.com','a9ea29611a175390e8c7d6b5d66645011506268142'),(83,28,'cris5580@gmail.com','c98e603cf232a5aa339337d40e24824d1506268144'),(84,29,'votoprog10@gmail.com','f04a52fded32668874f3c0ab6e1a51941506269624'),(85,29,'pab.quiros@gmail.com','5a487884a485d5fbd9a851fb96d6a1751506269626'),(86,30,'cris5580@gmail.com','8770139eebeefc2c234a7c10263c9a901506269816'),(87,30,'programathon2@gmail.com','01e5d07b0c6685d59bb53d2eb55027081506269818'),(88,30,'jupmasalamanca@gmail.com','39969033c72d1bcec5b862cc803211831506269819'),(89,30,'epiedra13@gmail.com','54288b9e44f93946fd062d83ddd3f9c91506269821'),(90,30,'argobas@gmail.com','8c143b0100ab806fd3d173a9cc94dd281506269822'),(91,30,'pab.quiros@gmail.com','44ed9548d14ef1485e8dc52c72c0248d1506269823'),(92,31,'pab.quiros@gmail.com','cd5f6d4c35f6fa2171ad9c547ca6f9471506270875'),(93,31,'epiedra13@gmail.com','39435d649c2db44bc50ab21f9a92f1bf1506270877'),(94,31,'programathon2@gmail.com','4d61e250029ba56f2045342a2ddeec891506270878'),(95,31,'cris5580@gmail.com\r\n','e93bd0ea74fbd3657d044541b5c4f5c61506270879'),(97,33,'programathon2@gmail.com','5df42795f392b21503a24cfe09caeb111506271573'),(98,34,'argps@tes.com','6a6803cccbfbb1dafe36e788071da4741506271670'),(99,34,'argpws@tes.com','6995cb5fecfe1f3c7b58dc8d8f6bae7c1506271671'),(100,34,'argps2@tes.com','e903ca78d59fc2d86fc1c2fed57bde051506271671'),(101,34,'epiedra13@gmail.com','e65ef758f37c380f17267cb51a9b09ee1506271671'),(102,34,'asdas@gasd.com','aa52bbcb31a27a1d674042b069e18a511506271671'),(103,35,'cris5580@gmail.com','edc349bed1c9c8d19430c8ebc1a7b3231506271776'),(104,37,'test@tas.com','593d8225458ddc965eb9edcc5e29dca41506271940'),(105,37,'tes2t@tas.com','e8ba5f0d353223d622e94d5da861466b1506271942'),(106,37,'testbbbb@tas.com','10a56dc2f2e577e4db29b2b28a628ca81506271943'),(107,37,'epiedra13@gmail.com','5b60804bb322e587eb0fc75fd5daed9c1506271944'),(108,37,'asdasd@tsts.com','c7676b2f6ca3c69617aa8cc7d06603e31506271945'),(109,36,'programathon2@gmail.com','e899b9aba67b7a4ab42a02afa143067d1506272034'),(110,38,'programathon2@gmail.com','f57a2a99876b3704c0573b50e2147b2f1506272409'),(111,39,'cris5580@gmail.com','379078d9ae93c65a386464af1639d9821506275319'),(112,40,'cris5580@gmail.com','972eb9b9b9e7510c9753e76bbadb8cb41506277367'),(113,41,'cris5580@gmail.com','130ab2a6bddc3fea71e1a4bead76ffd01506277795'),(114,42,'cris5580@gmail.com','65ac5a44056f05710c710025af94ccb81506277959'),(115,43,'cris5580@gmail.com','deb08a4a1f6fc3bdaf378d5a487d74701506278115'),(116,44,'pab.quiros@gmail.com','73ae14a8d7c22eb18fb8d2f889872df51506284207'),(117,44,'epiedra13@gmail.com','f960dd9a3925fc44502ceb42c1a74fdf1506284208'),(118,44,'programathon2@gmail.com','9f8597410ddb0a93f7e3c0ca74a6dab91506284209'),(119,44,'cris5580@gmail.com\r\n','7acd911e30cfa7a396c85dc246ebc89b1506284210'),(120,45,'programathon1@gmail.com','01319ec1acf872a2efc301a043dba9991506286589'),(121,46,'programathon1@gmail.com','e2d50b3b0d30c8b133631cf5468aadfd1506287255'),(122,46,'programathon2@gmail.com','62c3b46211354d4b7ff31e944d33e5141506287257'),(123,46,'programathon3@gmail.com','e8fc4a5c0ef84c832a6f103d669a8dd61506287257'),(124,46,'programathon4@gmail.com','95fceb49e245bfe065a38c763fe4e3bc1506287258'),(125,46,'programathon5@gmail.com','410b4c49f359931048fe864fb57ec8d51506287259'),(126,46,'programathon6@gmail.com','fb3bfce3c9d4efb0ecf7ced33cbadddd1506287260'),(127,46,'programathon7@gmail.com','be4db9f3d86990047aeb70d8b8ae58fa1506287262'),(128,46,'programathon8@gmail.com','1c3685bb64de1531dad005b2c268aaf01506287263'),(129,47,'programathon1@gmail.com','8c9875ad5b4dbc3d1d2c1ac15bb463c51506287409'),(130,47,'programathon2@gmail.com','6d3b33f501732e06be48ef9b5a8bb79a1506287411'),(131,47,'programathon3@gmail.com','4a09146119d57efb3a8aa209fa4211ed1506287412'),(132,47,'programathon4@gmail.com','5367e36b9329ca6989fedadec241d3db1506287413'),(133,47,'programathon5@gmail.com','6b14fa01054bf40b842f16c02529b05a1506287413'),(134,47,'programathon6@gmail.com','8ddc960e8de1ade1dc562309975fe3f11506287415'),(135,47,'programathon7@gmail.com','f7a6f2baca717fb58114afcae6d9292e1506287416'),(136,47,'programathon8@gmail.com','2f203bb7e070268bfbc73961428e73061506287417'),(137,48,'programathon1@gmail.com','7d077043e99f727c6642d54c53ef98391506288183'),(138,48,'programathon2@gmail.com','6059c2d24f1201eedd3a2e923e13bb1e1506288185'),(139,48,'programathon3@gmail.com','0ac5a36f2aab361293e5d04924869d7f1506288186'),(140,48,'programathon4@gmail.com','5b118ebe39ea433cb43b5907393f1ece1506288187'),(141,48,'programathon5@gmail.com','a7efa8c941fa1c617abdec5b4e64b5b01506288188'),(142,48,'programathon6@gmail.com','995efc3565a5d7ff0cb6c11a4dd655cf1506288189'),(143,48,'programathon7@gmail.com','880fc67b412d555a2dd39541902272441506288191'),(144,48,'programathon8@gmail.com','38f0044c61e31b1f7264f266a8d7107a1506288192');
/*!40000 ALTER TABLE `invitaciones` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `options_proceso`
--

DROP TABLE IF EXISTS `options_proceso`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `options_proceso` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `id_proceso_electoral` int(11) DEFAULT NULL,
  `pregunta` tinyint(1) DEFAULT '0',
  `opcion` varchar(255) DEFAULT NULL,
  `address` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `fk_options_proceso_ref_proceso_electoral` (`id_proceso_electoral`),
  CONSTRAINT `fk_options_proceso_ref_proceso_electoral` FOREIGN KEY (`id_proceso_electoral`) REFERENCES `proceso_electoral` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=198 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `options_proceso`
--

LOCK TABLES `options_proceso` WRITE;
/*!40000 ALTER TABLE `options_proceso` DISABLE KEYS */;
INSERT INTO `options_proceso` VALUES (21,2,0,'PAC1','9cbc7d14f41e53bc05e87b63fdb178dc'),(22,2,0,'PLN1','f8158d700b0a450c1441b808e1caec25'),(23,2,0,'PUSC1','2305eb9ff801f64d46ad0e4b2ccae34f'),(24,2,0,'Otros1','5367c0696403be41b21e396c7112bd49'),(25,3,0,'PAC1','8c1713cb41b2ce64e550f9b6e0c56b57'),(26,3,0,'PLN1','99489d3b6a1d616db30993372e8d9eea'),(27,3,0,'PUSC1','66fe6e796664d6818ed7b7821ad0fe5b'),(28,3,0,'Otros1','c0b65a3397a2016fc1c2d100370c17f8'),(29,4,1,'Como le fue1','cf9cfb6b82ca12e65e888649309c3e90'),(57,5,0,'Opc 1','57f669888b86b7310930f21de661fae2'),(58,5,0,'Opc 2','0e47165df9cf2602d2257ca0dab706d5'),(59,10,1,'Quien es?','29318c0ccaf99f0071e0772e51ad7f14'),(60,10,0,'SI','f66aee7521f2a1a4df15dd193fcf34d4'),(61,10,0,'NO','dbaf0230f6700fd68ed6648ab40a9ceb'),(62,11,1,'Sera q piedra termina? Sapbeeeee..','2f41ce554d892d947c6c5296cc9a315b'),(63,11,0,'SI','ad695bf845f60f6a866b7433276f536a'),(64,11,0,'NO','d36389d268725d46ba8031a419442b5e'),(68,13,0,'prueba','03dd4462bff34e5ab3b0e7c6f22f5059'),(69,13,0,'prueba 1','e0faf1ed0e48baf29687033cc38f385e'),(70,13,0,'prueba 2','cd8acbe00f7844b46b520cd07994f707'),(71,14,0,'prueba 2','988fe15951d6b0297f312f63706f5a76'),(72,14,0,'prueba 3','5adf98d2d9cd5f35fd197f9e4d125c65'),(73,14,0,'prueba 5','702edbf01ff1e2961462a9742b8181b7'),(74,14,0,'prueba 6','7b784bf6f156fbadceef218d5e1c6775'),(75,15,0,'sdfsd','d8b4e0a3c7add058e9803961f748f93a'),(76,15,0,'sfsd','b4660f195644bc58cca28e84142bc9f2'),(77,15,0,'dsf','276637835ba038c9c599bce7dc64a026'),(78,15,0,'sdf','0ac2595be9b7b3c80249ca4b71ebf5d7'),(79,16,1,'Te gusta esta prueba','19f1075229c54a39da87dcda187c275c'),(80,16,0,'SI','4fe080ecff575223416c3529fd238805'),(81,16,0,'NO','8f3474f61db4db1a01b684d9b73fb7bf'),(93,19,1,'Prueba','77c2ac741be6a0ecc98bb353839556de'),(94,19,0,'SI','277baca5ce7f3598bd5724a5272f781e'),(95,19,0,'NO','bdcee99abad5fa408eef41362004313c'),(96,20,1,'Prueba 3, esta seguro?','e575f1dd560acf871d3ca79baa1386bb'),(97,20,0,'SI','146174a3adcde70492846a407f8e5d58'),(98,20,0,'NO','a54527f6b69341aafa7a1c5c39d9e6b0'),(99,21,1,'q es?','3038c3a2614be22543f155edf0872bff'),(100,21,0,'SI','5f4ab92dfe8d4ed7f2fb643fe317a7c0'),(101,21,0,'NO','cdadb9c50824f5abaa2ea1cc48add5b4'),(102,22,1,'opa?','75bf382e5235ac782c21b9b913694076'),(103,22,0,'SI','03baa5790aacc0c84b8e21542ef20bff'),(104,22,0,'NO','4df9e5055dd5988519464f10aeae2e41'),(105,23,1,'uiyyy','2794bbdcaa76549e3f9b172abcef763f'),(106,23,0,'SI','25ce36476b47ea1a7a02ecbcc4169c6a'),(107,23,0,'NO','9673a4a4e69b78e448f103236bc8ad0b'),(108,24,1,'El test esta correcto?','d803a7ad12056de954539ede36054f7c'),(109,24,0,'SI','2bd7532398076ef78c894bdb415dc09e'),(110,24,0,'NO','f79ccc09f9e5a497a5d6f560f3c582dc'),(111,25,1,'asdadas','fc8683528ea119253d83e1afc291f93b'),(112,25,0,'SI','4489765d68af8d1831b52e35e89648ea'),(113,25,0,'NO','28cbf23301da195baafd42860ecb9cdf'),(114,26,1,'Esto es una pregunta','f51ecf52e190b009f8657f291a8b1582'),(115,26,0,'SI','96ef723ec28ff9631f39ff5d7a6fcdf8'),(116,26,0,'NO','2c814d58855609b6b297a2c6c20eb818'),(117,27,1,'Pregunta','8335a0e0d112a38af73fbbafac919c61'),(118,27,0,'SI','3e0ad7f626b6370f739e4af18d00d851'),(119,27,0,'NO','a19b600583f3009b5b88ea42e7d30178'),(120,28,1,'Pregunta a futuro','4a888547901ed796946b6a29c2aceb84'),(121,28,0,'SI','ac1eb9d065debedca30066648fba2382'),(122,28,0,'NO','bff992ffe54b7eea539371450bf5d27b'),(123,29,1,'Test002','9d341e655b2096e021ed95914330e726'),(124,29,0,'SI','fef79af4009ec4362860326e2cd9e880'),(125,29,0,'NO','f3887e66d28c2589e8b59e867ba74281'),(126,30,1,'Tienen suenio?','6c21ab809c48968e02369db8f07f6b53'),(127,30,0,'SI','91f07a0f5e513259ae52b44711cfb456'),(128,30,0,'NO','12357c2399c75d375c9d26b5c9af7914'),(129,31,0,'OP1','03867ed11b88f9fadb1b4246e36b1dad'),(130,31,0,'OP2','1a19e36de2e8c12b6ce3115e794ff7b5'),(131,31,0,'OP3','833f96667917dcbaf32e339ef057c7b5'),(136,33,0,'tttt','0b582b2bc5d023bddb7f6a06468991cb'),(137,33,0,'fffff','6ab0bb4794fdeed492b24dd1a5fe40f7'),(138,33,0,'jjjjj','ce69dd3d26fcfda70393a5280e07a4da'),(139,34,1,'testas','d19f3cc2f410f761a2bbec4d68dfb760'),(140,34,0,'SI','542d7a42147710eac105ed5e37329dad'),(141,34,0,'NO','5da9fd7f5664363c30ad57adb964b28d'),(142,35,0,'tttttt','5c8463452b9d26b3dd8ae834c37e63f9'),(143,35,0,'jjjjjjj','bc54b7762a6bd0992259abb24e6481bb'),(144,35,0,'iiiiiii','84f2c32249ad7e3ab2308223c3b213c3'),(145,35,0,'dddddd','5e5848c6ff8995df8dc6339b512c9a8d'),(146,37,1,'ultima prueba','053d383937fc20fb7f568070ffc0ef5f'),(147,37,0,'SI','388a1c17393a9fb8422763f45cba847a'),(148,37,0,'NO','36096a72843cfddd3a9587f0f07b9031'),(149,36,0,'123','7982593e7b7589ca350ebabefb4b5e55'),(150,36,0,'456','16535a15bcd48ac6d327e84e42f3d068'),(151,36,0,'789','44c4df5e6dec7271c682d9f19eaffba3'),(152,38,0,'1','97d0a841e030da7c5e4d68a478a81f44'),(153,38,0,'2','6799326a60a41a6aba4297ddd64a3578'),(154,38,0,'3','07bf7bdb8b954cad9597c7fad7955cfd'),(155,38,0,'4','9c88d3e4294b58d91da4b8d25645e3a7'),(156,39,1,'dsfsdfsdfds','830d9ec31450c166ba80f17b207dde52'),(157,39,0,'SI','20d1a987a2589cb7920fad6f43b4b156'),(158,39,0,'NO','9343d036f40ed51708414e7651dcc87a'),(159,40,0,'asdd','5ccd8cd88c9c2a444c5960899c4013ee'),(160,40,0,'sadasd','0bedf265b44cbba0b7da54d23f4c5dd8'),(161,40,0,'dddd','23c2d50af8d8d6194c329b07a749bd25'),(162,40,0,'asddasd','969a154211ea3018e1081528f720f9a0'),(163,41,0,'sadf','7037045d9843f2dd3108dec9afd2fbee'),(164,41,0,'asdfdsf','9565dec4d8777c5cd729508d074545d9'),(165,41,0,'dfgdfg','262dde0774bc95c6709534a76328d95f'),(166,41,0,'dfgfdfdfghf','0fb8d05c36e65c2c346085e92de68da9'),(167,41,0,'hjkhj','2b4e05161ed98c57836c17103b2a1699'),(168,42,1,'asdas','3632f9a7e7cbf10b0eab081c61fc1485'),(169,42,0,'SI','8b046bbddd9bc85e06df9c18c3aa73a4'),(170,42,0,'NO','d1da28fd0f7d5afafabe1cc0282f83b7'),(171,43,0,'sdfsd','2e92608a6b10408453934709d600f033'),(172,43,0,'sdf','976f2f0df729641f18808229a7f74a90'),(173,43,0,'sfdds','01bfc7492330810a7dd41d426748aa47'),(174,43,0,'sdf','976f2f0df729641f18808229a7f74a90'),(175,44,0,'prueba 1','e2c4ed63264cec303b2adf724346ef6d'),(176,44,0,'prueba 2','b4933706e493f56e666b8653c5a01ca3'),(177,44,0,'prueba 3','b52f643e8fe42c8ba705575ffa3d3252'),(178,45,0,'Opcion 1','fcfb3e60600b0ca4f3e32bcc315f4ef0'),(179,45,0,'Opcion 2','eee679fd9f0d44e6152daa32445eacff'),(180,46,1,'Voto si?','8b046271135e675ec937fa0c594e1573'),(181,46,0,'SI','b283c68ca41579c4c228410bee0715a0'),(182,46,0,'NO','5aed4810084bf1178d83b0c1935bee9d'),(183,47,0,'Opcion 1','44afbdb24a7eaec28b4b47555d4277a1'),(184,47,0,'Opcion 2','6ee33c8d9b7f676fc34c3dc115dc55c4'),(185,47,0,'Opcion 3','32c5cca71b7aa7290822701e6e917ff7'),(186,48,1,'Voto no?','ddb1c2e646955a36a4497bccb0d3f796'),(187,48,0,'SI','7cdfb093e1ced0652a81bdf49a976727'),(188,48,0,'NO','a910ccd1b2088bf8b950d3d76a6f71e0'),(189,12,0,'asdas','386cf0de7b62a475370d4353087648a0'),(190,12,0,'asdasd','cf7c0d6d7798b3f8d307b0ae795472e9'),(191,12,0,'sadasd','32bf38eb1400ffd84f39193a1c1cc297'),(195,50,1,'kjashd','d595f7eecf8486184c4754cf44b65001'),(196,50,0,'SI','0e8888f18daf2b8699ae5e9799408d94'),(197,50,0,'NO','5b96aa8fcba6458fd96bbe7598681622');
/*!40000 ALTER TABLE `options_proceso` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `proceso_electoral`
--

DROP TABLE IF EXISTS `proceso_electoral`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `proceso_electoral` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `status` varchar(30) NOT NULL,
  `fecha_inicio` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `fecha_final` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `tipo` varchar(30) DEFAULT NULL,
  `voto_publico` tinyint(1) DEFAULT NULL,
  `avances` tinyint(1) DEFAULT NULL,
  `nombre` varchar(255) NOT NULL,
  `ether_id` varchar(255) DEFAULT NULL,
  `min` int(3) DEFAULT NULL,
  `max` int(3) DEFAULT NULL,
  `id_admin` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `proceso_electoral`
--

LOCK TABLES `proceso_electoral` WRITE;
/*!40000 ALTER TABLE `proceso_electoral` DISABLE KEYS */;
INSERT INTO `proceso_electoral` VALUES (2,'finalizado','2017-09-18 10:55:00','2017-08-30 08:10:00','papeleta',1,0,'prueba','45',0,0,NULL),(3,'in_process','2017-09-18 10:55:00','2017-08-30 08:10:00','papeleta',1,0,'asdas','44',0,0,NULL),(4,'in_process','2017-09-18 10:55:00','2017-08-30 08:10:00','papeleta',1,0,'dghjhjgh','43',0,0,NULL),(5,'creando','2017-09-18 10:55:00','2017-09-21 05:45:00','papeleta',1,0,'Proceso 12','45',0,0,5),(10,'creando','2017-09-24 12:55:00','2017-09-25 12:50:00','referendum',1,0,'TestSexy','45',0,0,5),(11,'creando','2017-09-24 13:10:00','2017-09-25 13:35:00','referendum',1,1,'sexyyyyy','45',0,0,5),(12,'creando','2017-09-25 21:55:00','2017-09-26 20:50:00','papeleta',1,1,'sadasd test reg','45',0,0,5),(13,'creando','2017-09-24 13:35:00','2017-09-24 13:40:00','multiple',1,0,'prueba','45',2,3,5),(14,'creando','2017-09-24 13:40:00','2017-09-24 13:45:00','multiple',1,1,'prueba 234','45',2,3,5),(15,'creando','2017-09-24 14:00:00','2017-09-24 14:10:00','multiple',1,0,'Prueba 2','45',2,4,11),(16,'creando','2017-09-24 14:05:00','2017-09-24 14:10:00','referendum',0,1,'progra 3','45',0,0,11),(19,'finalizado','2017-09-24 14:21:24','2017-09-24 14:25:00','referendum',0,0,'prueba 5','45',0,0,11),(20,'creando','2017-09-24 14:35:00','2017-09-24 14:40:00','referendum',1,1,'test3','45',0,0,11),(21,'creando','2017-09-24 14:50:00','2017-09-24 14:55:00','referendum',1,0,'test03','45',0,0,11),(22,'creando','2017-09-24 15:00:00','2017-09-24 15:05:00','referendum',1,0,'T3','45',0,0,11),(23,'creando','2017-09-24 15:05:00','2017-09-24 15:10:00','referendum',1,1,'prueba cris','45',0,0,11),(24,'creando','2017-09-24 15:20:00','2017-09-24 15:35:00','referendum',0,0,'Test-003','45',0,0,11),(25,'creando','2017-09-24 15:35:00','2017-09-24 15:40:00','referendum',1,1,'prueba','45',0,0,11),(26,'creando','2017-09-24 15:40:00','2017-09-24 15:50:00','referendum',1,1,'Test-004','45',0,0,11),(27,'creando','2017-09-24 15:50:00','2017-09-24 15:55:00','referendum',0,0,'Test -003 v1','45',0,0,11),(28,'creando','2017-09-27 15:45:00','2017-09-30 15:45:00','referendum',0,0,'Test -003 v2','45',0,0,11),(29,'creando','2017-09-24 16:15:00','2017-09-24 16:20:00','referendum',0,1,'Test -003 v3','45',0,0,11),(30,'creando','2017-09-24 16:20:00','2017-09-24 16:25:00','referendum',1,0,'Test -003 v4','45',0,0,11),(31,'creando','2017-09-24 16:35:00','2017-09-24 16:40:00','papeleta',1,0,'Test -003 v5','45',0,0,11),(33,'creando','2017-09-24 16:45:00','2017-09-24 16:50:00','papeleta',1,0,'prueba 1','45',0,0,5),(34,'creando','2017-09-25 18:45:00','2017-09-29 16:50:00','referendum',1,0,'tsst emails','45',0,0,11),(35,'creando','2017-09-24 16:50:00','2017-09-24 16:50:00','papeleta',1,0,'prueba cris','45',0,0,5),(36,'creando','2017-09-24 16:55:00','2017-09-24 17:00:00','papeleta',1,0,'test12345','45',0,0,5),(37,'creando','2017-09-25 19:35:00','2017-09-25 23:55:00','referendum',1,0,'test ultimo','45',0,0,11),(38,'creando','2017-09-24 17:00:00','2017-09-24 17:05:00','multiple',1,1,'Prueba multiple','45',1,2,5),(39,'creando','2017-09-24 17:50:00','2017-09-25 18:00:00','referendum',0,1,'test ','45',0,0,11),(40,'creando','2017-09-24 18:25:00','2017-09-24 18:30:00','papeleta',1,1,'wwwwww','45',0,0,11),(41,'creando','2017-09-24 18:30:00','2017-09-24 18:35:00','multiple',1,1,'ffffff','45',1,2,11),(42,'creando','2017-09-24 18:35:00','2017-09-24 18:40:00','referendum',1,1,'asdasdsad','45',0,0,11),(43,'creando','2017-09-24 18:35:00','2017-09-24 18:40:00','papeleta',1,1,'prueba de correo','45',0,0,11),(44,'creando','2017-09-24 20:20:00','2017-09-24 20:25:00','papeleta',1,1,'prueba','45',0,0,11),(45,'creando','2017-09-24 20:55:00','2017-09-24 21:20:00','papeleta',1,1,'Regresion #1','45',0,0,11),(46,'creando','2017-09-24 21:10:00','2017-09-24 21:15:00','referendum',0,1,'Regresion #2','45',0,0,11),(47,'creando','2017-09-24 21:15:00','2017-09-24 21:20:00','multiple',1,0,'Regresion #3','45',2,3,11),(48,'creando','2017-09-24 21:25:00','2017-09-24 21:30:00','referendum',1,0,'Regresion #4','45',0,0,11),(50,'creando','2017-09-24 21:44:00','2017-09-27 21:30:00','referendum',1,1,'test ','45',0,0,5);
/*!40000 ALTER TABLE `proceso_electoral` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usuarios`
--

DROP TABLE IF EXISTS `usuarios`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usuarios` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tipoUsuario` int(11) NOT NULL DEFAULT '1',
  `correo` varchar(100) NOT NULL,
  `clave` varchar(100) NOT NULL,
  `nombre` varchar(100) DEFAULT NULL,
  `estado` int(2) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuarios`
--

LOCK TABLES `usuarios` WRITE;
/*!40000 ALTER TABLE `usuarios` DISABLE KEYS */;
INSERT INTO `usuarios` VALUES (5,1,'cris5580@gmail.com','dfe12303388e0a425b0550e6d3d09adf','Christian',2),(7,1,'jupmasalamanca@gmail.com','e0e8216a5ab686730fb2965bf791e411','MSA',1),(11,2,'programathon1@gmail.com','2c2e6c00c1b8850033a8555d3ddcbe96','Program Athon',2),(14,2,'programathon3@gmail.com','fbe45b3b9788398f4d78d95709ac58d0','Programathon 3!',1),(15,2,'programathon2@gmail.com','35aa139a2d3bd32f9cc60e598a5c720a','Programathon 2',1);
/*!40000 ALTER TABLE `usuarios` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `votar`
--

DROP TABLE IF EXISTS `votar`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `votar` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `token` varchar(255) DEFAULT NULL,
  `opcion` varchar(255) DEFAULT NULL,
  `id_proceso` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `votar`
--

LOCK TABLES `votar` WRITE;
/*!40000 ALTER TABLE `votar` DISABLE KEYS */;
INSERT INTO `votar` VALUES (1,'f8158d700b0a450c1441b808e1caab47','9cbc7d14f41e53bc05e87b63fdb178dc',2),(2,'f8158d790b0a450c1441b808e1caab47','9cbc7d14f41e53bc05e87b63fdb178dc',2),(3,'f8219d700b0a450c1441b808e1caab47','2305eb9ff801f64d46ad0e4b2ccae34f',2),(4,'efbef1d5f345349d7bb77c5b5ef060371506260255','702edbf01ff1e2961462a9742b8181b7',14),(5,'efbef1d5f345349d7bb77c5b5ef060371506260255','7b784bf6f156fbadceef218d5e1c6775',14),(6,'55a650d22bed5379b220ad5d02589e271506262481','0db698e512605d5ee51de7ed45cfd4cc',18),(7,'8b64e24a5d8a3baf9d10ab06ce9e23f51506262752','bdcee99abad5fa408eef41362004313c',19),(8,'ea536d1b5d973bea3d3c6e83cf42da281506264561','5f4ab92dfe8d4ed7f2fb643fe317a7c0',21),(9,'6a602dbdc697b179a57519230357c05d1506265569','25ce36476b47ea1a7a02ecbcc4169c6a',23),(10,'2f3e49d3a2d2de496418546542c5e16c1506265962','f79ccc09f9e5a497a5d6f560f3c582dc',24),(11,'779efc77d488ce6db4e40e7db9150c671506267462','2c814d58855609b6b297a2c6c20eb818',26),(12,'44ed9548d14ef1485e8dc52c72c0248d1506269823','91f07a0f5e513259ae52b44711cfb456',30),(13,'54288b9e44f93946fd062d83ddd3f9c91506269821','91f07a0f5e513259ae52b44711cfb456',30),(14,'2e958b34cfab84173c482c3812f450441506271117','80c2929ca7b742337f902f3db769266b',32),(15,'2e958b34cfab84173c482c3812f450441506271117','9455f3f80cd283de0b03f0ca305a88fb',32),(16,'e899b9aba67b7a4ab42a02afa143067d1506272034','7982593e7b7589ca350ebabefb4b5e55',36),(17,'f57a2a99876b3704c0573b50e2147b2f1506272409','97d0a841e030da7c5e4d68a478a81f44',38),(18,'f57a2a99876b3704c0573b50e2147b2f1506272409','07bf7bdb8b954cad9597c7fad7955cfd',38),(19,'379078d9ae93c65a386464af1639d9821506275319','20d1a987a2589cb7920fad6f43b4b156',39),(20,'972eb9b9b9e7510c9753e76bbadb8cb41506277367','0bedf265b44cbba0b7da54d23f4c5dd8',40),(21,'130ab2a6bddc3fea71e1a4bead76ffd01506277795','7037045d9843f2dd3108dec9afd2fbee',41),(22,'130ab2a6bddc3fea71e1a4bead76ffd01506277795','0fb8d05c36e65c2c346085e92de68da9',41),(23,'7acd911e30cfa7a396c85dc246ebc89b1506284210','b4933706e493f56e666b8653c5a01ca3',44),(24,'62c3b46211354d4b7ff31e944d33e5141506287257','b283c68ca41579c4c228410bee0715a0',46),(25,'6059c2d24f1201eedd3a2e923e13bb1e1506288185','a910ccd1b2088bf8b950d3d76a6f71e0',48);
/*!40000 ALTER TABLE `votar` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-09-24 15:55:03
