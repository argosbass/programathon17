CREATE TABLE `yoelijo`.`usuarios` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `tipoUsuario` INT NOT NULL DEFAULT 1,
  `correo` VARCHAR(100) NOT NULL,
  `clave` VARCHAR(100) NOT NULL,
  `nombre` VARCHAR(100) NOT NULL,
  `estado` INT(2) NOT NULL DEFAULT 1
  PRIMARY KEY (`id`));
  
  CREATE TABLE `yoelijo`.`invitaciones` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `correo` VARCHAR(100) NULL,
  `token` VARCHAR(100) NULL,
  PRIMARY KEY (`id`));
  
  ALTER TABLE `yoelijo`.`invitaciones` 
ADD COLUMN `id_proceso_electoral` INT NULL AFTER `id`,
ADD INDEX `fk_invitaciones_ref_proceso_electoral_idx` (`id_proceso_electoral` ASC);
ALTER TABLE `yoelijo`.`invitaciones` 
ADD CONSTRAINT `fk_invitaciones_ref_proceso_electoral`
  FOREIGN KEY (`id_proceso_electoral`)
  REFERENCES `yoelijo`.`proceso_electoral` (`ID`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;
