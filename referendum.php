<?php
  include_once('vendor/adodb/adodb.inc.php');
  include_once("vendor/config.php");

  $action = "";
  $errores  = array();
  $proceso_id = "";

  if ($_GET['tk']) {
    
    $token  = $_GET['tk'];

    $action = "referendum.php?tk=" . $token;

    $query  = "SELECT id_proceso_electoral FROM invitaciones WHERE token = '%s'";

    $sql = sprintf($query, $token );
    $sql = $yoelijo->Prepare($sql);

    $result = $yoelijo->Execute($sql) or DIE($yoelijo->ErrorMsg());

    $proceso_id = $result->fields['id_proceso_electoral'];

    $proceso  = "SELECT * FROM proceso_electoral WHERE ID = '%d'";
    $sql = sprintf($proceso, $proceso_id );

    $result = $yoelijo->Execute($sql) or DIE($yoelijo->ErrorMsg());

    $fecha_inicio = $result->fields['fecha_inicio'];
    $fecha_final  = $result->fields['fecha_final'];
    $fecha_actual = date('Y-m-d H:i:s');

    $avance = $result->fields['avances'];

  
    if ($fecha_inicio > $fecha_actual) {
      header('Location: mensage_estado.php?status=0');
    }

    if ($fecha_final < $fecha_actual) {
      header('Location: mensage_estado.php?status=1');
    }
    
    $nombre       = $result->fields['nombre'];

    $opciones  = "SELECT * FROM options_proceso WHERE id_proceso_electoral = '%d' && pregunta = '0'";
    $sql = sprintf($opciones, $proceso_id );

    $recordOPtions = $yoelijo->Execute($sql) or DIE($yoelijo->ErrorMsg());

    $opciones = array();

    while (!$recordOPtions->EOF) {

      $opciones[$recordOPtions->fields['address']]  = $recordOPtions->fields['opcion'];

      $recordOPtions->MoveNext();
    }

    $pregunta  = "SELECT * FROM options_proceso WHERE id_proceso_electoral = '%d' && pregunta = '1'";
    $sql = sprintf($pregunta, $proceso_id );

    $recordoPreg = $yoelijo->Execute($sql) or DIE($yoelijo->ErrorMsg());

    $preg = $recordoPreg->fields['opcion'];

  }

  if ($_POST['referendum']) {
    
    $voto = $_POST['referendum'];

      $check  = "SELECT * FROM votar WHERE token = '%s'";
      $sql = sprintf($check, $token);

      $checking = $yoelijo->Execute($sql) or DIE($yoelijo->ErrorMsg());

      if (empty($checking->fields)) {

          
          $votar  = "INSERT INTO votar (token,opcion,id_proceso) VALUES('%s', '%s', '%d')";
          $sql = sprintf($votar, $token, $voto, (int)$proceso_id );

          $yoelijo->Execute($sql) or DIE($yoelijo->ErrorMsg());

          $done[]  = "Su voto se ha registrado correctamente";

      }else{
        $errores[]  = "Usted ya ha votado, pongase en contacto con el administrados si cree que es un error";
      }

  }

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>getCode()</title>
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="css/business-frontpage.css" rel="stylesheet">
    <link href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css" rel="stylesheet">
        <link rel="stylesheet" href="https://www.amcharts.com/lib/3/plugins/export/export.css" type="text/css" media="all" />
    <style>
    .navbar-toggler {
        z-index: 1;
    }
    
    @media (max-width: 576px) {
        nav > .container {
            width: 100%;
        }
    }

label.btn span {
  font-size: 1.5em ;
}

label input[type="radio"] ~ i.fa.fa-circle-o{
    color: #c8c8c8;    display: inline;
}
label input[type="radio"] ~ i.fa.fa-dot-circle-o{
    display: none;
}
label input[type="radio"]:checked ~ i.fa.fa-circle-o{
    display: none;
}
label input[type="radio"]:checked ~ i.fa.fa-dot-circle-o{
    color: #7AA3CC;    display: inline;
}
label:hover input[type="radio"] ~ i.fa {
color: #7AA3CC;
}

label input[type="checkbox"] ~ i.fa.fa-square-o{
    color: #c8c8c8;    display: inline;
}
label input[type="checkbox"] ~ i.fa.fa-check-square-o{
    display: none;
}
label input[type="checkbox"]:checked ~ i.fa.fa-square-o{
    display: none;
}
label input[type="checkbox"]:checked ~ i.fa.fa-check-square-o{
    color: #7AA3CC;    display: inline;
}
label:hover input[type="checkbox"] ~ i.fa {
color: #7AA3CC;
}

div[data-toggle="buttons"] label.active{
    color: #7AA3CC;
}

div[data-toggle="buttons"] label {
display: inline-block;
padding: 6px 12px;
margin-bottom: 0;
font-size: 14px;
font-weight: normal;
line-height: 2em;
text-align: left;
white-space: nowrap;
vertical-align: top;
cursor: pointer;
background-color: none;
border: 0px solid 
#c8c8c8;
border-radius: 3px;
color: #c8c8c8;
-webkit-user-select: none;
-moz-user-select: none;
-ms-user-select: none;
-o-user-select: none;
user-select: none;
}

div[data-toggle="buttons"] label:hover {
color: #7AA3CC;
}

div[data-toggle="buttons"] label:active, div[data-toggle="buttons"] label.active {
-webkit-box-shadow: none;
box-shadow: none;
}

#chartdiv {
  width: 100%;
  height: 500px;
}

.amcharts-export-menu-top-right {
  top: 10px;
  right: 0;
}

    </style>

</head>

<body>

    <!-- Navigation -->
    <nav class="navbar fixed-top navbar-toggleable-md navbar-inverse bg-inverse">
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarExample" aria-controls="navbarExample" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="container">
            <a class="navbar-brand" href="#">Yo Elijo</a>
            <div class="collapse navbar-collapse" id="navbarExample"></div>
        </div>
    </nav>

    <form class="form" role="form" method="post" action="<?php echo $action;?>">
    <!-- Page Content -->
    <div class="container">

              <?php 
              if ( !empty($errores)) 
              {
                foreach ($errores as $error) 
                {
                  echo "<br>";
                  echo "<div class='alert alert-danger'>";
                  echo "<strong>Error!</strong> " . $error;
                  echo "</div>";
                }

              }
              ?>


              <?php 
              if ( !empty($done)) 
              {
                
                  echo "<br>";
                  echo "<div class='alert alert-success'>";
                  echo "<strong>Hecho!</strong> " . $done[0];
                  echo "</div>";

              }
              ?>

        <div class="row">
            <div class="col-md-6">
                <h4><?php echo $nombre;?></h4>
                <br>
                <div class="btn-group btn-group-vertical" data-toggle="buttons">
                    <?php echo $preg; ?>
                 

                    <?php foreach ($opciones as $key => $value) { ?>

                    <label class="btn">
                        <?php echo "<input type='radio' name='referendum' value='" . $key . "' id='" . $key . "' />"; ?>
                        <i class="fa fa-circle-o fa-2x"></i><i class="fa fa-dot-circle-o fa-2x"></i>
                        <?php echo "<span for='". $key . "'>" . trim($value) . "</span>"; ?>
                    </label>

                    <?php } ?>

                </div>
                <br>
                <input type="submit" name="votar" id="votar" class="btn btn-info" value="Votar">
            </div>
            <div class="col-md-6">
               <?php if ( $avance )  { 
                    $sql = sprintf("SELECT op.opcion,count(v.opcion) cantidad FROM votar v, options_proceso op where v.opcion = op.address and id_proceso = %s group by op.opcion",$proceso_id);
                    $sql = $yoelijo->Prepare($sql);
                    $Recordset	= $yoelijo->Execute($sql) or DIE($yoelijo->ErrorMsg());  
                    if($Recordset->EOF){
                        echo "Sin Datos";
                    }}?>
                <div id="chartdiv"></div>
            </div>
        </div>
        <!-- /.row -->

        <div>
          <?php 
            /*if ( $avance ) 
            {
              

              $avance  = "SELECT COUNT(opcion) as cantidad, opcion FROM votar WHERE id_proceso = '%s' GROUP BY opcion";

              $sql = sprintf($avance, $proceso_id );
              $sql = $yoelijo->Prepare($sql);

              $result = $yoelijo->Execute($sql) or DIE($yoelijo->ErrorMsg());

              while (!$result->EOF) {

                echo "<h4>";
                echo $opciones[$result->fields['opcion']]. ': ';
                echo $result->fields['cantidad'];
                echo "</h4>";
                echo "<br/>";

                $result->MoveNext();
              }
            
            }*/
          ?>
        </div>

    </div>
    <!-- /.container -->
    </form>

    <!-- Footer -->
    <footer class="py-5 bg-inverse">
        <div class="container">
            <p class="m-0 text-center text-white">Copyright &copy; Your Website 2017</p>
        </div>
        <!-- /.container -->
    </footer>

    <!-- Bootstrap core JavaScript -->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/tether/tether.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.min.js"></script>
    <script src="https://www.amcharts.com/lib/3/amcharts.js"></script>
    <script src="https://www.amcharts.com/lib/3/serial.js"></script>
    <script src="https://www.amcharts.com/lib/3/plugins/export/export.min.js"></script>
    <script src="https://www.amcharts.com/lib/3/themes/light.js"></script>
    <script src="js/web3.min.js"></script>
    <script src="js/truffle-contract.js"></script>
    <script src="js/app_voting.js"></script>

<?php  if ( $avance )  {?>
<script>
    var chart = AmCharts.makeChart("chartdiv", {
  "type": "serial",
  "theme": "light",
  "marginRight": 70,
    "dataProvider": [
    {
      "country": "<?php echo $Recordset->Fields("opcion");?>",
      "visits": <?php echo $Recordset->Fields("cantidad");?>,
      "color": "#0D52D1"
    }<?php $Recordset->MoveNext(); while (!$Recordset->EOF) { ?>   
    , {
      "country": "<?php echo $Recordset->Fields("opcion");?>",
      "visits": <?php echo $Recordset->Fields("cantidad");?>,
      "color": "#0D52D1"
    }<?php $Recordset->MoveNext();} ?>
    ],
  "startDuration": 1,
  "graphs": [{
    "balloonText": "<b>[[category]]: [[value]]</b>",
    "fillColorsField": "color",
    "fillAlphas": 0.9,
    "lineAlpha": 0.2,
    "type": "column",
    "valueField": "visits"
  }],
  "chartCursor": {
    "categoryBalloonEnabled": false,
    "cursorAlpha": 0,
    "zoomable": false
  },
  "categoryField": "country",
  "categoryAxis": {
    "gridPosition": "start",
    "labelRotation": 45
  },
  "export": {
    "enabled": true
  }

});
</script>

<?php } ?>
</body>

</html>
