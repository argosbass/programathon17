<?php  
	
  include_once("seguridad.php");
  include_once('vendor/adodb/adodb.inc.php');
  include_once("vendor/config.php");

  if (session_status() == PHP_SESSION_NONE) 
  {
    session_start();
  } 

    $_SESSION['getEther_id']   = "";

    if( $_POST['LastIdProposal'])
    {
        $_SESSION['getEther_id']   =   $_POST['LastIdProposal'];

        $query = "UPDATE proceso_electoral SET ether_id = '%s' WHERE ID = '%s'";
        $sql = sprintf($query, $_POST['LastIdProposal'], $_SESSION['process_id']);
        $sql = $yoelijo->Prepare($sql);

        $results =  $yoelijo->Execute($sql) or DIE($yoelijo->ErrorMsg());

    }

	function displayEmails($emails){
		$email_list = "";
        $_SESSION["emails-count"] = 0;
		foreach ($emails as $value) {
			$email_list .= "<div>" . $value . "</div>";
		}
		$_SESSION['email-list'] = $email_list;
	}

    // Get Proceso id.
    function getProcesoTipo(){
      GLOBAL $yoelijo;

        $query = "SELECT tipo FROM proceso_electoral WHERE ID = '%s'";
        $sql = sprintf($query, $_SESSION['process_id']);
        $sql = $yoelijo->Prepare($sql);

        $results =  $yoelijo->Execute($sql) or DIE($yoelijo->ErrorMsg());
        return $results->fields['tipo'];
    }

    function getProcesoNombre(){
      GLOBAL $yoelijo;

        $query = "SELECT nombre FROM proceso_electoral WHERE ID = '%s'";
        $sql = sprintf($query, $_SESSION['process_id']);
        $sql = $yoelijo->Prepare($sql);

        $results =  $yoelijo->Execute($sql) or DIE($yoelijo->ErrorMsg());
        return $results->fields['nombre'];
    }


	function genMailUrlToken($email){
		return md5(session_id() . time() . $email) . time();
	}

	function sendCorreo($mail2, $email, $link, $nombre){
        $mail2->IsSMTP();
        $mail2->SMTPDebug  = 0;
        $mail2->Host       = 'smtp.gmail.com';
        $mail2->Port       = 587;
        $mail2->SMTPSecure = 'tls';
        $mail2->SMTPAuth   = true;
        $mail2->Username = 'getcode.cr@gmail.com';
        $mail2->Password = 'getcode.123';
        $mail2->From = 'getcode.cr@gmail.com';
        $mail2->FromName = "YoElijo";
        $mail2->clearAddresses();
        $mail2->AddAddress($email);
        $mail2->Subject = "YoElijo - ".$nombre;
        $mail2->Body = "<h4>Ha sido seleccionado para participar en nuestro proceso, el cual consiste en una pregunta, su opinion es muy importante para nosotros.</h4>" . $link;
        $mail2->Send();
	}

	// Envia todos los correos.
	function sendemails($email_list){
		require("vendor/email/class.phpmailer.php");
        require("vendor/email/class.smtp.php");
		$mail2 = new PHPMailer();
        GLOBAL $yoelijo;
//$yoelijo->debug=1;
            $query = "SELECT * FROM proceso_electoral WHERE ID = '%s'";
            $sql = sprintf($query, $_SESSION['process_id']);
            $sql = $yoelijo->Prepare($sql);
            $results =  $yoelijo->Execute($sql) or DIE($yoelijo->ErrorMsg());
            
        foreach ($email_list as $value) {
                    $token = genMailUrlToken($value);
                    $nombre = $results->Fields("nombre");
                    $link = "Proceso: ".$results->Fields("nombre")."<br>";
                    $link .= "Fechas: ".$results->Fields("fecha_inicio")." al ".$results->Fields("fecha_final")."<br>";
                    $link .= "Tipo votacion: ".$results->Fields("tipo")."<br>";
                    $link .= "Tipo voto: ";
                    if($results->Fields("voto_publico")==1){
                        $link .= "Publico";
                    }else{
                        $link .= "Privado";
                    }
                    $link .="<br>";
                    $link .= "Con Avances: ";
                    if($results->Fields("avances")==1){
                        $link .= "Si";
                    }else{
                        $link .= "No";
                    }
                    $link .="<br>";
                    $link .=  "<a href=\"http://" . '192.168.128.40' . '/programathon2017/' . getProcesoTipo() .'.php?tk=' . $token."\">Dar click al link para ingresar al proceso</a>";
            //echo $link;
                    sendCorreo($mail2, $value, $link, $nombre);

            // Guarda el correo en BD.
            $sql = sprintf("INSERT INTO invitaciones (correo,token,id_proceso_electoral)VALUES('%s','%s','%s')",$value,$token,$_SESSION['process_id']);           
           $sql = $yoelijo->Prepare($sql);           
           $RecordsetInsert = $yoelijo->Execute($sql) or DIE($yoelijo->ErrorMsg());
        }
        unset($_SESSION['process_id']);
        $_POST['textarea-tokens'] = NULL;
        header('Location: listaProceso.php?msj=3');

	}

	if(!empty($_FILES['emails-list']["type"])){

		$errors= array();
		$file_name = $_FILES['emails-list']['name'];
		$file_size =$_FILES['emails-list']['size'];
		$file_tmp =$_FILES['emails-list']['tmp_name'];
		$file_type=$_FILES['emails-list']['type'];
		$file_ext=strtolower(end(explode('.',$_FILES['emails-list']['name'])));      
		$expensions= array("txt");

		if(in_array($file_ext,$expensions)=== false){
		 	$errors[]="extension not allowed, please choose a .txt file.";
		}

		if($file_size > 2097152){
		 	$errors[]='File size must be excately 2 MB';
		}

		if(empty($errors)==true){
			 move_uploaded_file($file_tmp,"files/".$file_name);
			 echo "Su archivo se ha subido de manera exitosa!!";
		} else{
			 print_r($errors);
		}

		$mails = file_get_contents('files/'.$file_name);
		$mails = explode(",", $mails);
        $_SESSION["emails-count"] = count($mails);
		//displayEmails($mails);
		sendemails($mails);
    }

    $nombre = "";
    if ($_SESSION['process_id'])
    {

    $query = "SELECT nombre FROM proceso_electoral WHERE ID = '%s'";
    $sql = sprintf($query, $_SESSION['process_id']);

    $sql = $yoelijo->Prepare($sql);

    $result = $yoelijo->Execute($sql) or DIE($yoelijo->ErrorMsg());

    if ($result->fields['nombre']) 
    {
      $nombre = $result->fields['nombre'];
    }

  }

    if (!empty($_POST['input_email']) ){
    	$email_list = json_decode($_POST['input_email']);
        $_SESSION["emails-count"] = count($email_list);
        //print_r($_SESSION["emails-count"]);
    	sendemails($email_list);
    }

?>

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Business Frontpage - Start Bootstrap Template</title>

    <!-- Bootstrap core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/business-frontpage.css" rel="stylesheet">

    <!-- Temporary navbar container fix -->
    <style>
    .navbar-toggler {
        z-index: 1;
    }
    
    @media (max-width: 576px) {
        nav > .container {
            width: 100%;
        }
    }

    #upload-list-form input, #upload-list-form .upload-list-description {
		margin-left: 15px;
	}


    #upload-list-form #emails-list {
    	padding-left: 0px;
    }

    #upload-list-form {
    	padding-top: 20px;
    	padding-bottom: 20px;
    }

    #upload-list-form .upload-list-description {
    	margin-top: 20px;
    }

 

    </style>
</head>

<body>

    <!-- Navigation -->
    <nav class="navbar fixed-top navbar-toggleable-md navbar-inverse bg-inverse">
        <div class="container">
            <a class="navbar-brand" href="#">yoelijo</a>
            
                <ul class="navbar-nav ml-auto col-ms-2 menu-header">
                    <li class="nav-item">
                        <a class="nav-link" href="#">Logout</a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>

    <!-- Header with Background Image -->
    <header class="business-header">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="display-3 text-white mt-4"><?php print $nombre; ?></h1>
                </div>
            </div>
        </div>
    </header>

    <!-- Page Content -->
    <div class="container">
    	

    	<form id="upload-list-form" role="form" method="post" action="" enctype="multipart/form-data">




      <?php  

        $query = "SELECT fecha_inicio, fecha_final, voto_publico FROM proceso_electoral WHERE ID = '%s'";
        $sql = sprintf($query, $_SESSION['process_id']);
        $sql = $yoelijo->Prepare($sql);

        $results =  $yoelijo->Execute($sql) or DIE($yoelijo->ErrorMsg());


        $options = "SELECT address FROM options_proceso WHERE id_proceso_electoral = '%s'";
        $sql = sprintf($options, $_SESSION['process_id']);
        $sql = $yoelijo->Prepare($sql);

        $resultsopts =  $yoelijo->Execute($sql) or DIE($yoelijo->ErrorMsg());
        
        $opciones    = "";

        while (!$resultsopts->EOF) {

            $opciones  .= " 0x".$resultsopts->fields['address'] . ",";

            $resultsopts->MoveNext();
        }

        $opciones    =  rtrim($opciones,",");
        $opciones   .=  "";

        $fi   = strtotime($results->fields['fecha_inicio']);

        $ff   = strtotime($results->fields['fecha_final']);

        $tipo_voto   = $results->fields['voto_publico'];
?>

        <input type="hidden" value="<?php echo $fi;?>" id="f_inicial_h"/>

        <input type="hidden" value="<?php echo $ff;?>" id="f_final_h"/>

        <input type="hidden" value="<?php echo $tipo_voto;?>" id="publico"/>

        <input type="hidden" value="<?php echo $opciones;?>" id="opciones"/>


    <?php
    if( $_SESSION['getEther_id'] )
    {
    ?>
    <input type="hidden" id="LastIdProposal" name="LastIdProposal" value = "<?php echo $_SESSION['getEther_id']; ?>"/>
    <?php }else{ ?>
        <input type="hidden" value="" name="LastIdProposal" id="LastIdProposal"/>
    <?php } ?>

    		<div class="emails-container">
    		<div class='form-group'>
				<div class='col-sm-4'>
					<h4 for='input_email'>Ingrese las direcciones de correo electronico</h4>
					<input type='text' id='input_email' name='input_email' class='form-control'>
				</div>
			</div>
    	</div>
    	<p style="margin-left: 15px">También puede subir un archivo con las direcciones de correo.</p>
			  <input type="file" name="emails-list" id="emails-list">
			  <p class="upload-list-description">La extencion permitida es .txt, el formato del archivo es,las direcciones de correco separadas por una coma(,), por ejemplo: direccion_de_correo1,direccion_de_correo2</p>
			  <input type="submit" name= ="subir-correos" value="Enviar Correos" class="btn btn-default">
                          
		</form>
		<div>			
			<?php print (isset($_SESSION['email-list']))? "<h2>Emails Uploaded</h2>" . $_SESSION['email-list'] : "";?>
		</div>
    <!-- /.container -->

    <!-- Footer -->
    <footer class="py-5 bg-inverse">
        <div class="container">
            <p class="m-0 text-center text-white">Copyright &copy; Your Website 2017</p>
        </div>
        <!-- /.container -->
    </footer>

    <!-- Bootstrap core JavaScript -->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/tether/tether.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.min.js"></script>
    <script type='text/javascript' src="js/programathon17.js"></script>
    <script type='text/javascript' src="js/multiple-emails.js"></script>
    <script type="text/javascript" src='http://code.jquery.com/jquery-latest.min.js'></script>
    <link type="text/css" rel="stylesheet" href="css/multiple-emails.css" />
    <script src="js/web3.min.js"></script>
    <script src="js/truffle-contract.js"></script>
    
    <?php
    if( $_SESSION['getEther_id'] )
    {
    ?>
    <script src="js/app_voting_tokens.js"></script>
    <?php
    }else{
    ?>
    <script src="js/app_voting.js"></script>
    <?php   
    } 
    ?>
    



</body>

</html>
