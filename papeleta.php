<?php
  include_once('vendor/adodb/adodb.inc.php');
  include_once("vendor/config.php");

  $action = "";
  $errores  = array();
  $proceso_id = "";

  if ($_GET['tk']) {
    
    $token  = $_GET['tk'];

    $action = "papeleta.php?tk=" . $token;

    $query  = "SELECT id_proceso_electoral FROM invitaciones WHERE token = '%s'";

    $sql = sprintf($query, $token );
    $sql = $yoelijo->Prepare($sql);

    $result = $yoelijo->Execute($sql) or DIE($yoelijo->ErrorMsg());

    $proceso_id = $result->fields['id_proceso_electoral'];

    $proceso  = "SELECT * FROM proceso_electoral WHERE ID = '%d'";
    $sql = sprintf($proceso, $proceso_id );

    $result = $yoelijo->Execute($sql) or DIE($yoelijo->ErrorMsg());

    $fecha_inicio = $result->fields['fecha_inicio'];
    $fecha_final  = $result->fields['fecha_final'];
    $fecha_actual = date('Y-m-d H:i:s');

    $avance = $result->fields['avances'];

  
    if ($fecha_inicio > $fecha_actual) {
      header('Location: mensage_estado.php?status=0');
    }

    if ($fecha_final < $fecha_actual) {
      header('Location: mensage_estado.php?status=1');
    }

    $nombre       = $result->fields['nombre'];

    $opciones  = "SELECT * FROM options_proceso WHERE id_proceso_electoral = '%d'";
    $sql = sprintf($opciones, $proceso_id );

    $recordOPtions = $yoelijo->Execute($sql) or DIE($yoelijo->ErrorMsg());

    $opciones = array();

    while (!$recordOPtions->EOF) {

      $opciones[$recordOPtions->fields['address']]  = $recordOPtions->fields['opcion'];

      $recordOPtions->MoveNext();
    }

  }


  if ($_POST['papeleta']) {
    
    $voto = $_POST['papeleta'];

      $check  = "SELECT * FROM votar WHERE token = '%s'";
      $sql = sprintf($check, $token);

      $checking = $yoelijo->Execute($sql) or DIE($yoelijo->ErrorMsg());

      if (empty($checking->fields)) {

          
          $votar  = "INSERT INTO votar (token,opcion,id_proceso) VALUES('%s', '%s', '%d')";
          $sql = sprintf($votar, $token, $voto, (int)$proceso_id );

          $yoelijo->Execute($sql) or DIE($yoelijo->ErrorMsg());

          $done[]  = "Su voto se ha registrado correctamente";

      }else{
        $errores[]  = "Usted ya ha votado, pongase en contacto con el administrados si cree que es un error";
      }

  }

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Votacion de papeleta</title>
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="css/business-frontpage.css" rel="stylesheet">
    <link rel="stylesheet" href="https://www.amcharts.com/lib/3/plugins/export/export.css" type="text/css" media="all" />
    <style>
    .navbar-toggler {
        z-index: 1;
    }
    
    @media (max-width: 576px) {
        nav > .container {
            width: 100%;
        }
    }
    @import('https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.0/css/bootstrap.min.css') 

.funkyradio div {
  clear: both;
  overflow: hidden;
}

.funkyradio label {
  width: 100%;
  border-radius: 3px;
  border: 1px solid #D1D3D4;
  font-weight: normal;
}

.funkyradio input[type="radio"]:empty,
.funkyradio input[type="checkbox"]:empty {
  display: none;
}

.funkyradio input[type="radio"]:empty ~ label,
.funkyradio input[type="checkbox"]:empty ~ label {
  position: relative;
  line-height: 2.5em;
  text-indent: 3.25em;
  margin-top: 2em;
  cursor: pointer;
  -webkit-user-select: none;
     -moz-user-select: none;
      -ms-user-select: none;
          user-select: none;
}

.funkyradio input[type="radio"]:empty ~ label:before,
.funkyradio input[type="checkbox"]:empty ~ label:before {
  position: absolute;
  display: block;
  top: 0;
  bottom: 0;
  left: 0;
  content: '';
  width: 2.5em;
  background: #D1D3D4;
  border-radius: 3px 0 0 3px;
}

.funkyradio input[type="radio"]:hover:not(:checked) ~ label,
.funkyradio input[type="checkbox"]:hover:not(:checked) ~ label {
  color: #888;
}

.funkyradio input[type="radio"]:hover:not(:checked) ~ label:before,
.funkyradio input[type="checkbox"]:hover:not(:checked) ~ label:before {
  content: '\2714';
  text-indent: .9em;
  color: #C2C2C2;
}

.funkyradio input[type="radio"]:checked ~ label,
.funkyradio input[type="checkbox"]:checked ~ label {
  color: #777;
}

.funkyradio input[type="radio"]:checked ~ label:before,
.funkyradio input[type="checkbox"]:checked ~ label:before {
  content: '\2714';
  text-indent: .9em;
  color: #333;
  background-color: #ccc;
}

.funkyradio input[type="radio"]:focus ~ label:before,
.funkyradio input[type="checkbox"]:focus ~ label:before {
  box-shadow: 0 0 0 3px #999;
}

.funkyradio-default input[type="radio"]:checked ~ label:before,
.funkyradio-default input[type="checkbox"]:checked ~ label:before {
  color: #333;
  background-color: #ccc;
}

.funkyradio-primary input[type="radio"]:checked ~ label:before,
.funkyradio-primary input[type="checkbox"]:checked ~ label:before {
  color: #fff;
  background-color: #337ab7;
}

.funkyradio-success input[type="radio"]:checked ~ label:before,
.funkyradio-success input[type="checkbox"]:checked ~ label:before {
  color: #fff;
  background-color: #5cb85c;
}

.funkyradio-danger input[type="radio"]:checked ~ label:before,
.funkyradio-danger input[type="checkbox"]:checked ~ label:before {
  color: #fff;
  background-color: #d9534f;
}

.funkyradio-warning input[type="radio"]:checked ~ label:before,
.funkyradio-warning input[type="checkbox"]:checked ~ label:before {
  color: #fff;
  background-color: #f0ad4e;
}

.funkyradio-info input[type="radio"]:checked ~ label:before,
.funkyradio-info input[type="checkbox"]:checked ~ label:before {
  color: #fff;
  background-color: #5bc0de;
}

#chartdiv {
  width: 100%;
  height: 500px;
}

    </style>

</head>

<body>

    <!-- Navigation -->
    <nav class="navbar fixed-top navbar-toggleable-md navbar-inverse bg-inverse">
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarExample" aria-controls="navbarExample" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="container">
            <a class="navbar-brand" href="#">Yo Elijo</a>
            <div class="collapse navbar-collapse" id="navbarExample"></div>
        </div>
    </nav>

    <form class="form" role="form" method="post" action="<?php echo $action;?>">
    <!-- Page Content -->
    <div class="container">

              <?php 
              if ( !empty($errores)) 
              {
                foreach ($errores as $error) 
                {
                  echo "<br>";
                  echo "<div class='alert alert-danger'>";
                  echo "<strong>Error!</strong> " . $error;
                  echo "</div>";
                }

              }
              ?>


              <?php 
              if ( !empty($done)) 
              {
                
                  echo "<br>";
                  echo "<div class='alert alert-success'>";
                  echo "<strong>Hecho!</strong> " . $done[0];
                  echo "</div>";

              }
              ?>
        <div class="row">
            <div class="col-md-6">
                <h4><?php echo $nombre;?></h4>

                <div class="funkyradio">

                    <?php foreach ($opciones as $key => $value) { ?>

                    <div class="funkyradio-success">
                        <?php echo "<input type='radio' name='papeleta' value='" . $key . "' id='" . $key . "' />"; ?>
                        <?php echo "<label for='". $key . "'>" . trim($value) . "</label>"; ?>
                    </div>

                    <?php } ?>

                </div>
                <input type="submit" name="votar" id="votar" class="btn btn-info" value="Votar">
            </div>
            <div class="col-md-6">
                               <?php if ( $avance )  { 
                    $sql = sprintf("SELECT op.opcion,count(v.opcion) cantidad FROM votar v, options_proceso op where v.opcion = op.address and id_proceso = %s group by op.opcion",$proceso_id);
                    $sql = $yoelijo->Prepare($sql);
                    $Recordset	= $yoelijo->Execute($sql) or DIE($yoelijo->ErrorMsg());  
                    if($Recordset->EOF){
                        echo "Sin Datos";
                               }}?>
                <div id="chartdiv"></div>
            </div>
        </div>
        <!-- /.row -->

        <div>
          <?php 
            /*if ( $avance ) 
            {
              

              $avance  = "SELECT COUNT(opcion) as cantidad, opcion FROM votar WHERE id_proceso = '%s' GROUP BY opcion";

              $sql = sprintf($avance, $proceso_id );
              $sql = $yoelijo->Prepare($sql);

              $result = $yoelijo->Execute($sql) or DIE($yoelijo->ErrorMsg());

              while (!$result->EOF) {

                echo "<h4>";
                echo $opciones[$result->fields['opcion']]. ': ';
                echo $result->fields['cantidad'];
                echo "</h4>";
                echo "<br/>";

                $result->MoveNext();
              }
            
            }*/
          ?>
        </div>

    </div>
    <!-- /.container -->
    </form>

    <!-- Footer -->
    <footer class="py-5 bg-inverse">
        <div class="container">
            <p class="m-0 text-center text-white">Copyright &copy; Your Website 2017</p>
        </div>
        <!-- /.container -->
    </footer>

    <!-- Bootstrap core JavaScript -->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/tether/tether.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.min.js"></script>
    <script src="https://www.amcharts.com/lib/3/amcharts.js"></script>
    <script src="https://www.amcharts.com/lib/3/pie.js"></script>
    <script src="https://www.amcharts.com/lib/3/plugins/export/export.min.js"></script>
    <script src="https://www.amcharts.com/lib/3/themes/light.js"></script>
    <script src="js/web3.min.js"></script>
    <script src="js/truffle-contract.js"></script>
    <script src="js/app_voting.js"></script>

    <?php  if ( $avance )  { ?>
    <script>
    var chart = AmCharts.makeChart( "chartdiv", {
        "type": "pie",
        "theme": "light",
        "dataProvider": [
        {
          "country": "<?php echo $Recordset->Fields("opcion");?>",
          "litres": <?php echo $Recordset->Fields("cantidad");?>
        }<?php $Recordset->MoveNext(); while (!$Recordset->EOF) { ?>   
        , {
          "country": "<?php echo $Recordset->Fields("opcion");?>",
          "litres": <?php echo $Recordset->Fields("cantidad");?>
        }<?php $Recordset->MoveNext();} ?>
        ],
        "valueField": "litres",
        "titleField": "country",
         "balloon":{
         "fixedPosition":true
        },
        "export": {
          "enabled": true
        }
      } );

    </script>
    <?php } ?>
</body>

</html>
