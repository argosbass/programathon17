pragma solidity ^0.4.10;


contract owned {
    address public owner;

    function owned()  {
        owner = msg.sender;
    }

    modifier onlyOwner {
        require(msg.sender == owner);
        _;
    }

    function transferOwnership(address newOwner) onlyOwner  {
        owner = newOwner;
    }
}
 
contract Voting is owned {
    
    struct Proposal 
    {
        uint proposalID;

        uint startDate;
        uint endDate;
        // 0 => público; // 1 => privado; 
        uint voteType;
        
        uint votesReceived;
        uint votesTotal;
        
        bool active;
        
        bytes32[] tokens;
        bytes32[] tokensUsed;

        mapping (address => uint) counter;
        
        address[] options;

    }
    
    // Contract Variables and proposals
   
    Proposal [] proposals;
    uint public numProposals;
    uint public lengthProposals;
    
 

    // Constructor
    function Voting( ) payable  {
        
    }
 
    function newProposal (
        address[]  options,
        uint startDate,
        uint endDate,
        uint voteType
    )
       onlyOwner 
       returns (uint proposalID)
    {

        proposalID = proposals.length++;
        Proposal storage e = proposals[proposalID];
        
        e.options      = options;
        e.startDate     = startDate;
        e.endDate       = endDate;
        e.voteType      = voteType;
        e.active = true; //validar además que sea el dia de inicio
         
        numProposals = proposalID+1;

        return proposalID;
 
    }
    
    function newToken(uint proposalID, uint cantTokens) {
        
        require(validProposalID(proposalID)==true);
         
         Proposal storage e = proposals[proposalID];
         
        uint start = 0;
        uint stop = cantTokens;
         
        if( e.tokens.length > 0){ 
            start = e.tokens.length; 
            stop = e.tokens.length + cantTokens;
        }
         
          
        for (uint i = start; i<stop ; i++){ 
          
          e.tokens.length++;
          e.tokens[i] = generateToken();
          
        }
   
    }

    
  
    function newVote (
        uint proposalID,
        address optionAddress, 
        uint date,
        bytes32 userToken
    )  
        returns (uint voteID)
    {
        Proposal storage p = proposals[proposalID];         // find the proposal
 
        require(date >= p.startDate && date <= p.endDate);  // requere vote bewteen valid dates
        
        require(p.active == true);                          // requere proposal active
        
        for (uint i = 0; i<p.tokens.length; i++){ 
          
          if(p.tokens[i] == userToken && userToken != 0 ){
            
            
             p.tokensUsed.length++;
             p.tokensUsed[p.tokensUsed.length-1]=p.tokens[i];
             
             p.tokens[i]=0x0000000000000000000000000000000000000000000000000000000000000000; 
             p.votesReceived++;  
             if(p.votesReceived == p.votesTotal){
                p.active = false;
             }
             
             p.counter[optionAddress] += 1; 
          
          }
          
        }
    
 
        return p.votesReceived;
    }
    
   
  
    function  getTokens( 
    uint proposalID
   ) 
   returns (bytes32[] tokens)
   {
    
        require(validProposalID(proposalID)==true);
         
        Proposal storage e = proposals[proposalID];
        
       return e.tokens;
   }
   
    function  getTokensUsed( 
    uint proposalID
   ) 
   returns (bytes32[] tokensUsed)
   {
    
        require(validProposalID(proposalID)==true);
         
        Proposal storage e = proposals[proposalID];
        
       return e.tokensUsed;
   }

 
   
   function generateToken() public returns (bytes32){
       uint256 lastBlockNumber = block.number - 1;
       lengthProposals  = lastBlockNumber;
       bytes32 hashVal = bytes32(block.blockhash(lastBlockNumber));
       return bytes32(hashVal);
   }
   
   
   
    function getResultsByOption(uint proposalID , address option)  returns (uint)  {
        require (validOption(proposalID,option) == true) ;
        
        Proposal storage e = proposals[proposalID];
        
        return e.counter[option];
    }
 
 
    function validOption( uint proposalID , address option )  returns (bool) 
    {
        Proposal storage e = proposals[proposalID];
        
        for(uint i = 0; i < e.options.length; i++) {
            if (e.options[i] == option) {
                return true;
            }
        }
        return false;
    }
    
    
    function validProposalID( uint proposalID  )  returns (bool) 
    {
        if(proposalID < proposals.length) { 
            return true; 
        }
        return false;
    }


  function getLastIdProposal() public constant returns(uint) {
        return numProposals-1;
    }
    

     
}




