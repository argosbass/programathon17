<?php 
include_once("seguridad.php");
include_once('vendor/adodb/adodb.inc.php');
include_once("vendor/config.php");
//$yoelijo->debug=1;
 $error = 0;
 if ( $_POST['f_inicial'] && $_POST['f_final'] ) {

  $errores    = array();

  $f_inicial  = $_POST['f_inicial'];
  $f_final    = $_POST['f_final'];

  if ($f_inicial < $f_actual ) {

    $errores[]  = "La fecha inicial: " . $f_inicial . " no debe de ser menor a la fecha actual: " . $f_actual;
    $error = 1;
  }

  if ($f_inicial > $f_final ) {
    $errores[]  = "La fecha inicial: " . $f_inicial . " no debe de ser mayor a la fecha final: " . $f_final;
    $error = 1;
  }

}
if($_POST['id']!="" and $error == 0){
    //echo "entro<br>";
    if(isset($_POST['editar'])){
        //echo "entro1<br>";
        $publico = $_POST['publico'];
        $avances = $_POST['avances'];
        $num_min = $_POST['num_min'];
        $num_max = $_POST['num_max'];

        if($_POST['publico']==""){
            $publico = 0;
        }
        if($_POST['avances']==""){
            $avances = 0;
        }
        if($_POST['num_min']==""){
            $num_min = 0;
        }
        if($_POST['num_max']==""){
            $num_max = 0;
        }

        $inicial = explode("-", $_POST['f_inicial']);
        $final = explode("-", $_POST['f_final']);

        $Finicial = explode("/", $inicial[0]);
        $Ffinal = explode("/", $final[0]);

        $Hinicial = explode(":", $inicial[1]);
        $Hfinal = explode(":", $final[1]);

        $fechaInicio = trim($Finicial[2])."-".trim($Finicial[1])."-".trim($Finicial[0])." ".trim($Hinicial[0]).":".trim($Hinicial[1]).":00";
        $fechaFinal  = trim($Ffinal[2])."-".trim($Ffinal[1])."-".trim($Ffinal[0])." ".trim($Hfinal[0]).":".trim($Hfinal[1]).":00";

        $sql = "UPDATE proceso_electoral SET nombre = '".$_POST['nombre_proceso']."',fecha_inicio = '$fechaInicio',fecha_final = '$fechaFinal',tipo = '".$_POST['tipo']."',voto_publico = $publico,avances = $avances,min = $num_min,max = $num_max where id = ".$_POST['id'];
        $sql = $yoelijo->Prepare($sql);
        $RecordsetUpdate = $yoelijo->Execute($sql) or DIE($yoelijo->ErrorMsg());  
        
        $sql = sprintf("DELETE FROM options_proceso where id_proceso_electoral = %s",$_POST['id']);
        $sql = $yoelijo->Prepare($sql);
        $RecordsetDeleteOption = $yoelijo->Execute($sql) or DIE($yoelijo->ErrorMsg());
        
        $pregunta = 0;
        if($_POST['tipo']=="papeleta"){
            $options = $_POST['papeleta_options'];
        }
        if($_POST['tipo']=="referendum"){
            $pregunta = 1;
            $options[0] = $_POST['refendum_option'];
        }
        if($_POST['tipo']=="multiple"){
            $options = $_POST['multiple_options'];
        }

        foreach ($options as $clave => $valor){
            if($valor!=""){
                $token = md5($_SESSION['process_id'].$valor.date('d/m/Y H:i:s'));
                $sql = sprintf("INSERT INTO options_proceso(id_proceso_electoral,pregunta,opcion,address) VALUES (%s,%s,'%s','%s')",$_POST['id'],$pregunta,$valor,$token);
                $sql = $yoelijo->Prepare($sql);
                $RecordsetInsert = $yoelijo->Execute($sql) or DIE($yoelijo->ErrorMsg());
            }
        }
        //header('Location: listaProceso.php?msj=2');
    }
}

if($_REQUEST['id']!=""){
    $fecha_actual = date('Y-m-d H:i:s');
    $sql = "SELECT *,DATE_FORMAT(fecha_inicio, '%d/%m/%Y - %H:%i') fecha_inicioF,DATE_FORMAT(fecha_final, '%d/%m/%Y - %H:%i') fecha_finalF from proceso_electoral where id = ".$_REQUEST['id']." and '".$fecha_actual."' <= fecha_inicio";
    $sql = $yoelijo->Prepare($sql);
    $Recordset	= $yoelijo->Execute($sql) or DIE($yoelijo->ErrorMsg());
    
    if($Recordset->EOF){
        $error = 2;
    }
    /*$fecha_inicio = $Recordset->fields['fecha_inicio'];
    $fecha_final  = $Recordset->fields['fecha_final'];
    $fecha_actual = date('Y-m-d H:i:s');
  
    echo $fecha_inicio." >= ".$fecha_actual." and ".$fecha_final." <= ".$fecha_actual;
    if ($fecha_inicio >= $fecha_actual and $fecha_final <= $fecha_actual) {
        $error = 2;
    }*/

    /*if () {
       $error = 2;
    }*/
    if($error==0){
        $sql = "SELECT * from options_proceso where id_proceso_electoral = ".$_REQUEST['id'];
        $sql = $yoelijo->Prepare($sql);
        $RecordsetOpciones	= $yoelijo->Execute($sql) or DIE($yoelijo->ErrorMsg());

        if($_POST['id']!=""){
            $id = $_POST['id'];
        }else{
            $id = $Recordset->Fields("ID");
        }

        if($_POST['f_inicial']!=""){
            $f_inicial = $_POST['f_inicial'];
        }else{
            $f_inicial = $Recordset->Fields("fecha_inicioF");
        }
        if($_POST['f_final']!=""){
            $f_final = $_POST['f_final'];
        }else{
            $f_final = $Recordset->Fields("fecha_finalF");
        }
        $nombre = $Recordset->Fields("nombre");
        $voto_publico = $Recordset->Fields("voto_publico");
        $avances = $Recordset->Fields("avances");
        $tipo = $Recordset->Fields("tipo");
        $status = $Recordset->Fields("status");
        $min = $Recordset->Fields("min");
        $max = $Recordset->Fields("max");
    }
}else{
    header('Location: index.php');
}

?>

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>getCode()</title>

    <!-- Bootstrap core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-datetimepicker.min.css" rel="stylesheet" media="screen">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">

    <!-- Custom styles for this template -->
    <link href="css/business-frontpage.css" rel="stylesheet">

    <!-- Temporary navbar container fix -->
    <style>
    .navbar-toggler {
        z-index: 1;
    }
    
	
    footer {
     /* position: absolute;
      bottom: 0;*/
      width: 100%; 
    }
    
   .admin-proceso {
     margin-top: 10%; 
   }

   .continuar {
      text-align: center;
      margin-top: 30px;
   }
   
   .menu-header {
     float:right;
   } 

   .form-inline label{
    display: inline;
   }
   	
   img.card-img-top.img-fluid {
    width: 100%;
    height: 200px;
   }

   .card-block{
    text-align: center;
   }

   .hide{
    display: none;
   }

   .form-group.show_error input ,
   #box-referendum.show_error input,
   #box-multiple input.show_error{
      border: solid 2px red;
   }



    @media (max-width: 576px) {
      .display-4{
        font-size: 200%;
      }
    }
 
    @media (max-width: 576px) {
        nav > .container {
            width: 100%;
        }
    }
    </style>

</head>

<body>

    <!-- Navigation -->
    <nav class="navbar fixed-top navbar-toggleable-md navbar-inverse bg-inverse">
        <div class="container">
            <a class="navbar-brand" href="#">yoelijo</a>
            
                <ul class="navbar-nav ml-auto col-ms-2 menu-header">
                    <?php include_once("menu.php"); ?>
                </ul>
            </div>
        </div>
    </nav>

    <!-- Page Content -->
    <div class="container">
            
                <?php if($error!=2){ ?>
        <div class="col-sm-12">
              <?php 
              if ( !empty($errores)) 
              {
                foreach ($errores as $error) 
                {
                  echo "<br>";
                  echo "<div class='alert alert-danger'>";
                  echo "<strong>Error!</strong> " . $error;
                  echo "</div>";
                }

              }
              ?>


    <form class="form admin-proceso" role="form" method="post" action="">
    <div class="form-group">
        <input type="text" class="form-control" id="nombre-proceso" name="nombre_proceso" placeholder="Nombre Proceso" value="<?php echo $nombre; ?>" required>
    </div>
    <div class="form-inline block-horas">
      <div class="form-group col-md-6">
        <label for="dtp_input1" class="col-md-12 control-label">Fecha y Hora de Inicio</label>
      
        <div class="input-group date form_datetime col-md-12" data-date-format="dd/mm/yyyy - hh:ii" data-link-field="dtp_input1">
            <input class="form-control" size="16" name="f_inicial" type="text" value="<?php echo $f_inicial;?>" readonly required>
            
            <span class="input-group-addon">
              <span class="fa fa-calendar"></span>
            </span>

        </div>  
        <input type="hidden" id="dtp_input1" value="" /><br/>
      </div>

      <div class="form-group col-md-6">
        <label for="dtp_input1" class="col-md-12 control-label">Fecha y Hora Final</label>
        
        <div class="input-group date form_datetime col-md-12" data-date-format="dd/mm/yyyy - hh:ii" data-link-field="dtp_input2">
            <input class="form-control" size="16" type="text" name="f_final" value="<?php echo $f_final;?>" readonly required>
            
            <span class="input-group-addon">
              <span class="fa fa-calendar"></span>
            </span>

        </div>  
        <input type="hidden" id="dtp_input2" value="" /><br/>
      </div>  		

    </div>

    <hr>


    <div class="row">
            <?php if($tipo=="papeleta"){ ?>
            <div class="col-sm-4 my-4">
                <div class="card">
                    <img class="card-img-top img-fluid" src="css/img/votacion.png" alt="">
                    <div class="card-block">
                        <h4 class="card-title">Papeleta</h4>
                    </div>
                    <div class="card-footer">
                        <!--<input type="radio"  name="tipo" value="papeleta" class="form-control" id="papeleta" checked="checked"/>-->
                    </div>
                </div>
            </div>
            <?php } ?>
            <?php if($tipo=="referendum"){ ?>
            <div class="col-sm-4 my-4">
                <div class="card">
                    <img class="card-img-top img-fluid" src="css/img/referendum.jpg" alt="">
                    <div class="card-block">
                        <h4 class="card-title">Referendum</h4>
                    </div>
                    <div class="card-footer">
                        <!--<input type="radio"  name="tipo" value="referendum"" class="form-control" id="referendum"/>-->
                    </div>
                </div>
            </div>
            <?php } ?>
            <?php if($tipo=="multiple"){ ?>
            <div class="col-sm-4 my-4">
                <div class="card">
                    <img class="card-img-top img-fluid" src="css/img/multiple.jpg" alt="">
                    <div class="card-block">
                        <h4 class="card-title">Opcion Multiple</h4>
                    </div>
                    <div class="card-footer">
                        <!--<input type="radio"  name="tipo" value="multiple" class="form-control" id="multiple"/>-->
                    </div>
                </div>
            </div>
            <?php } ?>
        </div>
        <!-- /.row -->

        <hr>
        <input type="hidden" name="tipo" value="<?php echo $tipo; ?>"/>
        <input type="hidden" name="id" value="<?php echo $id; ?>"/>
        
        <?php if($tipo=="papeleta"){ ?>
        <div id = "box-papeleta">
          <h3 class="control-label">Agregar Opciones para Papeleta</h3>

          <div class="form-group form-inline">
              <div class="col-xs-5">
                  <input type="text" class="form-control" name="papeleta_options[]" value="<?php echo $RecordsetOpciones->Fields("opcion");?>"/>
                  <?php $RecordsetOpciones->MoveNext(); ?>  
              </div>
              <div class="col-xs-4">
                  <button type="button" class="btn btn-default addButton"><i class="fa fa-plus"></i></button>
              </div>
          </div>
        <?php while (!$RecordsetOpciones->EOF) { ?>  
        <div class="form-group form-inline">
            <div class="col-xs-offset-3 col-xs-5">
                <input class="form-control" type="text" name="papeleta_options[]" value="<?php echo $RecordsetOpciones->Fields("opcion");?>">
            </div>
            <div class="col-xs-4">
                <button type="button" class="btn btn-default removeButton"><i class="fa fa-minus"></i></button>
            </div>
        </div>
        <?php $RecordsetOpciones->MoveNext();} ?>   
        <!-- The option field template containing an option field and a Remove button -->
        <div class="form-group form-inline hide" id="optionTemplate">
            <div class="col-xs-offset-3 col-xs-5">
                <input class="form-control" type="text" name="papeleta_options[]"/>
            </div>
            <div class="col-xs-4">
                <button type="button" class="btn btn-default removeButton"><i class="fa fa-minus"></i></button>
            </div>
        </div>

      </div>
      <?php } ?>
     <?php if($tipo=="referendum"){ ?>
      <div id = "box-referendum">

      <h3 class="control-label">Agregar una pregunta o propuesta para el Referendum</h3>
        <input type="textarea" class="col-md-12" name="refendum_option" style="height: 100px" value="<?php echo $RecordsetOpciones->Fields("opcion");?>" />
      </div>
     <?php } ?> 
     <?php if($tipo=="multiple"){ ?>
      <div id = "box-multiple">

          <h3 class="control-label">Agregar Opciones</h3>

          <div class="form-group form-inline">
              <div class="col-xs-5 col-md-5"  style="padding-left:0px">
                  <input type="text" class="form-control" name="multiple_options[]" value="<?php echo $RecordsetOpciones->Fields("opcion");?>"/>
                  <?php $RecordsetOpciones->MoveNext(); ?>  
              </div>
          </div>

          <div class="form-group form-inline">
              <div class="col-xs-5">
                  <input type="text" class="form-control" name="multiple_options[]" value="<?php echo $RecordsetOpciones->Fields("opcion");?>"/>
                  <?php $RecordsetOpciones->MoveNext(); ?>  
              </div>
              <div class="col-xs-4">
                  <button type="button" class="btn btn-default addButtonmultiple"><i class="fa fa-plus"></i></button>
              </div>
          </div>
          
        <?php while (!$RecordsetOpciones->EOF) { ?>  
        <div class="form-group form-inline">
            <div class="col-xs-offset-3 col-xs-5">
                <input class="form-control" type="text" name="multiple_options[]" value="<?php echo $RecordsetOpciones->Fields("opcion");?>">
            </div>
            <div class="col-xs-4">
                <button type="button" class="btn btn-default removeButton"><i class="fa fa-minus"></i></button>
            </div>
        </div>
        <?php $RecordsetOpciones->MoveNext();} ?>   
          
          
        <!-- The option field template containing an option field and a Remove button -->
        <div class="form-group form-inline hide" id="multipleoptionTemplate">
            <div class="col-xs-offset-3 col-xs-5">
                <input class="form-control" type="text" name="multiple_options[]"/>
            </div>
            <div class="col-xs-4">
                <button type="button" class="btn btn-default removeButton"><i class="fa fa-minus"></i></button>
            </div>
        </div>



          <div class="form-group col-md-6">
            <label for="dtp_input1" class="col-md-12 control-label">Opciones minimas a elegir</label>
            <div class="col-xs-5 col-md-5">
                <input type="number" class="form-control" name="num_min" value="<?php echo $min; ?>"/>
            </div>
          </div>

            <div class="form-group col-md-6">
              <label for="dtp_input1" class="col-md-12 control-label">Opciones maximas a elegir</label>
              <div class="col-xs-5 col-md-5">
                  <input type="number" class="form-control" name="num_max" value="<?php echo $max; ?>"/>
              </div>
            </div>
          </div>
        <?php } ?>
      <hr>

        <div class="form-inline check-options col-md-12">

          <div class="checkbox col-md-6 form-control">
            <label>
                <input type="checkbox" name="publico" value="1" <?php if($voto_publico==1){ ?>checked<?php } ?>/> Voto Publico
            </label>
          </div>

          <div class="checkbox col-md-6 form-control">
            <label>
              <input type="checkbox" name="avances" value="1" <?php if($avances==1){ ?>checked<?php } ?>/> Avances
            </label>
          </div>

        </div>

        <div class="form-group continuar">
               <div class="col-sm-offset-2 col-sm-10">
                    <input type="submit" name="editar" id="editar" class="btn btn-success" value="Guardar">
                    <a href="./listaProceso.php" class="btn btn-danger" role="button">Cancelar</a>
               </div>
       </div>	
	</form>
        </div>
                <?php }else{ ?>
                            <div class="col-md-12">
                <div class="alert alert-danger">
                  <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
                  <strong>Validaci&oacute;n!</strong> Solo puede editar un proceso miestra este abierto.
                </div>
                                        <div class="form-group continuar">
               <div class="col-sm-offset-2 col-sm-10">
                    <a href="./listaProceso.php" class="btn btn-danger" role="button">Cancelar</a>
               </div>
            </div>
        <?php } ?>
        <!-- /.row -->

    </div>
    <!-- /.container -->

    <!-- Footer -->
    <footer class="py-5 bg-inverse">
        <div class="container">
            <p class="m-0 text-center text-white">Copyright &copy; Your Website 2017</p>
        </div>
        <!-- /.container -->
    </footer>

    <!-- Bootstrap core JavaScript -->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/tether/tether.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/bootstrap-datetimepicker.js" charset="UTF-8"></script>
    <script type="text/javascript" src="js/locales/bootstrap-datetimepicker.fr.js" charset="UTF-8"></script>
    <script type='text/javascript' src="js/add_options.js"></script>
    <script type='text/javascript' src="js/programathon17.js"></script>
    <script type='text/javascript' src="js/multiple-emails.js"></script>
  
    <script type="text/javascript">
    $('.form_datetime').datetimepicker();
  </script>

</body>

</html>
