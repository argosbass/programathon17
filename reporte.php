<?php
include_once("seguridad.php");
include_once('vendor/adodb/adodb.inc.php');
include_once("vendor/config.php");
//$yoelijo->debug=1;
$error = 0;
if($_GET['id']!=""){
    $fecha_actual = date('Y-m-d H:i:s');
    $sql = "SELECT status,voto_publico from proceso_electoral where id = ".$_GET['id']." and fecha_final <= '".$fecha_actual."'";
    $sql = $yoelijo->Prepare($sql);
    $RecordsetP	= $yoelijo->Execute($sql) or DIE($yoelijo->ErrorMsg());
    if(!$RecordsetP->EOF){
        $sql = sprintf("SELECT op.opcion,count(v.opcion) cantidad FROM votar v, options_proceso op where v.opcion = op.address and id_proceso = %s group by op.opcion",$_GET['id']);
        $sql = $yoelijo->Prepare($sql);
        $Recordset	= $yoelijo->Execute($sql) or DIE($yoelijo->ErrorMsg());

        if($RecordsetP->Fields("voto_publico")==1){
            $sql = sprintf("SELECT i.correo,op.opcion FROM votar v, options_proceso op, invitaciones i where v.opcion = op.address and v.token = i.token and id_proceso = %s",$_GET['id']);
            $sql = $yoelijo->Prepare($sql);
            $RecordsetList	= $yoelijo->Execute($sql) or DIE($yoelijo->ErrorMsg());
        }
    }else{
        $error = 1;
    }
}
?><!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>getCode()</title>
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="css/business-frontpage.css" rel="stylesheet">
    <link rel="stylesheet" href="https://www.amcharts.com/lib/3/plugins/export/export.css" type="text/css" media="all" />
    <style>
    .navbar-toggler {
        z-index: 1;
    }
    
    @media (max-width: 576px) {
        nav > .container {
            width: 100%;
        }
    }

#chartdiv {
  width: 100%;
  height: 500px;
}

    </style>

</head>

<body>

    <!-- Navigation -->
    <nav class="navbar fixed-top navbar-toggleable-md navbar-inverse bg-inverse">
        <div class="container">
            <a class="navbar-brand" href="#">yoelijo</a>
            
                <ul class="navbar-nav ml-auto col-ms-2 menu-header">
                    <?php include_once("menu.php"); ?>
                </ul>
            </div>
        </div>
    </nav>

    <!-- Page Content -->
    <div class="container">

        <div class="row">
         <?php if($error==1){ ?>
            <div class="col-md-12">
                <div class="alert alert-danger">
                  <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
                  <strong>Validaci&oacute;n!</strong> Solo puede generar reporte cuando el proceso esta cerrado.
                </div>
            </div>
        <?php }else{ ?>
            <div class="col-md-6">
                <div id="chartdiv"></div>
            </div>
            <?php if($RecordsetP->Fields("voto_publico")==1){ ?>
            <div class="col-md-6">
                <h1>Lista Votantes</h1>
                <br>
                  <div class="table-responsive">
                    <table id="datatable1" class="table table-striped table-bordered" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th>Correo</th>
                                <th>Voto</th>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                                <th>Correo</th>
                                <th>Voto</th>
                            </tr>
                        </tfoot>
                        <tbody>
                            <?php while (!$RecordsetList->EOF) { ?>
                            <tr>
                                <td><?php echo $RecordsetList->Fields("correo");?></td>
                                <td><?php echo $RecordsetList->Fields("opcion");?></td>
                            </tr>
                            <?php $RecordsetList->MoveNext();} ?> 
                        </tbody>
                    </table>
                </div>                      
            </div>
            <?php } ?>
           <?php } ?>
            <div class="form-group continuar">
               <div class="col-sm-offset-2 col-sm-10">
                    <a href="./listaProceso.php" class="btn btn-danger" role="button">Atras</a>
               </div>
            </div>
        </div>
        <!-- /.row -->

    </div>
    <!-- /.container -->

    <!-- Footer -->
    <footer class="py-5 bg-inverse">
        <div class="container">
            <p class="m-0 text-center text-white">Copyright &copy; Your Website 2017</p>
        </div>
        <!-- /.container -->
    </footer>

    <!-- Bootstrap core JavaScript -->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/tether/tether.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.min.js"></script>
    <script src="https://www.amcharts.com/lib/3/amcharts.js"></script>
    <script src="https://www.amcharts.com/lib/3/pie.js"></script>
    <script src="https://www.amcharts.com/lib/3/plugins/export/export.min.js"></script>
    <script src="https://www.amcharts.com/lib/3/themes/light.js"></script>
    <script src="js/web3.min.js"></script>
    <script src="js/truffle-contract.js"></script>
    <script src="js/app_voting.js"></script>
    
    <script>
    var chart = AmCharts.makeChart( "chartdiv", {
        "type": "pie",
        "theme": "light",
        "dataProvider": [
        {
          "country": "<?php echo $Recordset->Fields("opcion");?>",
          "litres": <?php echo $Recordset->Fields("cantidad");?>
        }<?php $Recordset->MoveNext(); while (!$Recordset->EOF) { ?>   
        , {
          "country": "<?php echo $Recordset->Fields("opcion");?>",
          "litres": <?php echo $Recordset->Fields("cantidad");?>
        }<?php $Recordset->MoveNext();} ?>
        ],
        "valueField": "litres",
        "titleField": "country",
         "balloon":{
         "fixedPosition":true
        },
        "export": {
          "enabled": true
        }
      } );
    </script>
</body>

</html>
